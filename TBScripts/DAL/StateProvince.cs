//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class StateProvince
    {
        public StateProvince()
        {
            this.Customers = new HashSet<Customer>();
            this.Sites = new HashSet<Site>();
        }
    
        public string StateId { get; set; }
        public string StateName { get; set; }
    
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Site> Sites { get; set; }
    }
}
