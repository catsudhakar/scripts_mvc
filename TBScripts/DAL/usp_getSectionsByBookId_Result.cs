//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    
    public partial class usp_getSectionsByBookId_Result
    {
        public int SectionId { get; set; }
        public string SectionTitle { get; set; }
        public int BookId { get; set; }
        public Nullable<int> SortSeq { get; set; }
        public string BookTitle { get; set; }
    }
}
