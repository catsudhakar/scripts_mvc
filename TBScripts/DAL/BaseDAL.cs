﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace TBScripts.DAL
{
    public class BaseDAL
    {
        private string connectionString;

        public BaseDAL()
        {
            this.connectionString = this.GetConnectionString();
        }

        /// <summary>
        /// The GetConnectionString method is used to get the connection string from Application configuration file.
        /// </summary>
        /// <returns></returns>
        public string GetConnectionString()
        {
            string connectionString= ConfigurationManager.ConnectionStrings["PHdataConnectionString2"].ToString();
            return connectionString;
          //  return "Data Source =10.10.12.132;Initial Catalog=PHdata_NewDb;User Id=sudhakar;Password=sudha123?;";
        }

        /// <summary>
        /// The GetConnection method is used to create the new instance of MySqlConnection and open connection.
        /// </summary>
        /// <returns>MySqlConnection instance</returns>
        public SqlConnection GetConnection()
        {
            SqlConnection sqlConnection;
            sqlConnection = new SqlConnection(this.connectionString);
            sqlConnection.Open();
            return sqlConnection;
        }

        /// <summary>
        /// The GetConnectionForDataSet method is used to create the new instance of MySqlConnection.
        /// </summary>
        /// <returns>MySqlConnection instance</returns>
        public SqlConnection GetConnectionForDataSet()
        {
            SqlConnection sqlConnection;
            sqlConnection = new SqlConnection(this.connectionString);
            return sqlConnection;
        }
    }
}
