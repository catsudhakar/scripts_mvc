﻿
using System;
using System.Collections.Generic;
using System.Text;
using TBScripts.Models;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Configuration;

using Microsoft.ApplicationBlocks.Data;
using System.Collections;

namespace TBScripts.DAL
{
    public class eFolderDal : BaseDAL
    {
        public List<eFolderModel> GeteFolderList(int siteid)
        {

            //Declaring List of objects type Person
            List<eFolderModel> lstPerson = null;

            if (HttpContext.Current.Session["Person"] != null)
            {
                PersonModel objPerson = (PersonModel)HttpContext.Current.Session["Person"];
            }

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstPerson = new List<eFolderModel>();

                    //Defining SQL Procedures
                    string strSPName = "usp_getSiteFolderTree";
                    SqlParameter sqlParam = new SqlParameter("@SiteId", siteid);
                    

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParam);

                    //Filling PersonList from drData
                    this.FilleFolderList(ref lstPerson, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstPerson;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        #region FilleFolderList List
      
        private void FilleFolderList(ref List<eFolderModel> lstPerson, SqlDataReader drData)
        {

            //Checking whether drData has records or not
            if (drData.HasRows) // if dtData has records 
            {
                //Navigating to all dtData records to fill data
                while (drData.Read())
                {
                    eFolderModel objPerson = new eFolderModel();
                    //List<FolderFiles> lstFolderFiles = null;
                    objPerson.folderfileslist = null;

                    if (drData["SiteId"] != DBNull.Value)
                    {
                        objPerson.SiteId = Convert.ToInt32(drData["SiteId"]);
                    }
                    if (drData["FolderId"] != DBNull.Value)
                    {
                        objPerson.FolderId = Convert.ToInt32(drData["FolderId"]);
                        objPerson.folderfileslist = GeteFolderListByFolderId(objPerson.FolderId);
                    }
                    if (drData["FolderName"] != DBNull.Value)
                    {
                        objPerson.FolderName = Convert.ToString(drData["FolderName"]);
                    }
                    if (drData["FolderSortSeq"] != DBNull.Value)
                    {
                        objPerson.SortSeq = Convert.ToInt32(drData["FolderSortSeq"]);
                    }
                   
                    //Adding Person to the BE Object
                    lstPerson.Add(objPerson);
                }
            }

        }

        #endregion

        #region "get files"

        private void FilleFolderFilesList(ref List<FolderFiles> lstPerson, SqlDataReader drData)
        {

            //Checking whether drData has records or not
            if (drData.HasRows) // if dtData has records 
            {
                //Navigating to all dtData records to fill data
                while (drData.Read())
                {
                    FolderFiles objPerson = new FolderFiles();
                    if (drData["Name"] != DBNull.Value)
                    {
                        objPerson.Name = Convert.ToString(drData["Name"]);
                    }
                    if (drData["Id"] != DBNull.Value)
                    {
                        objPerson.Id = Convert.ToInt32(drData["Id"]);

                    }
                    if (drData["Keywords"] != DBNull.Value)
                    {
                        objPerson.Keyword = Convert.ToString(drData["Keywords"]);
                    }
                    if (drData["Type"] != DBNull.Value)
                    {
                        objPerson.Type = Convert.ToString(drData["Type"]);
                    }

                    //Adding Person to the BE Object
                    lstPerson.Add(objPerson);
                }
            }

        }
        public List<FolderFiles> GeteFolderListByFolderId(int FolderId)
        {

            //Declaring List of objects type Person
            List<FolderFiles> lstfolderFiles = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstfolderFiles = new List<FolderFiles>();

                    //Defining SQL Procedures
                    string strSPName = "GetFilesForFolder";
                    SqlParameter sqlParam = new SqlParameter("@folderId", FolderId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParam);

                    //Filling PersonList from drData
                    this.FilleFolderFilesList(ref lstfolderFiles, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstfolderFiles;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        #endregion

        #region "Update efoleder sort order"
        public void UpdateefolderSeqOrder(string strHandOutIdArr, string strhSeqOrder)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "Update_efolder_SeqOrder";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@FolderIdArr", strHandOutIdArr);
                sqlParams[1] = new SqlParameter("@hSeqOrder", strhSeqOrder);
                

                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);

                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }
        #endregion

        public object GetFileTypeBasedOnRole(string spname,Hashtable parameters)
        {
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {
                try
                {
                    SqlParameter[] sqlParams = new SqlParameter[parameters.Count];
                    int i = 0;
                    foreach (string n in parameters.Keys)
                    {
                        sqlParams[i] = new SqlParameter(n, parameters[n]);
                        i++;
                    }
                    object o = SqlHelper.ExecuteScalar(sqlConn, System.Data.CommandType.StoredProcedure, spname, sqlParams);
                    sqlConn.Close();
                    return o;
                }
                finally
                {
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region "FileNotes_GetAll"
        public void GetFileNotesByFileID(ref NotesModel objNotesModel)
        {
            
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                   //Defining SQL Procedures
                    string strSPName = "FileNotes_GetAll";
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@fileId", objNotesModel.FileId);
                    sqlParams[1] = new SqlParameter("@siteid", objNotesModel.SiteId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    this.FillNotes(ref objNotesModel, drData);

                   
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        private void FillNotes(ref NotesModel objNotesModel, SqlDataReader drData)
        {

            if (drData.HasRows)
            {
                if (drData.Read())
                {
                    if (drData["id"] != DBNull.Value)
                    {
                        objNotesModel.Id = Convert.ToInt32(drData["id"]);
                    }
                    if (drData["Text"] != DBNull.Value)
                    {
                        objNotesModel.Text = Convert.ToString(drData["Text"]);
                    }
                    if (drData["LastUpdated"] != DBNull.Value)
                    {
                        objNotesModel.LastUpdateDate = Convert.ToDateTime(drData["LastUpdated"]);
                    }
                    if (drData["IsDeleted"] != DBNull.Value)
                    {
                        objNotesModel.isDeleted = Convert.ToBoolean(drData["IsDeleted"]);
                    }
                    if (drData["description"] != DBNull.Value)
                    {
                        objNotesModel.Description = Convert.ToString(drData["description"]);
                    }

                    if (drData["SiteId"] != DBNull.Value)
                    {
                        objNotesModel.SiteId = Convert.ToInt32(drData["SiteId"]);
                    }
                    if (drData["fileId"] != DBNull.Value)
                    {
                        objNotesModel.FileId = Convert.ToInt32(drData["fileId"]);
                    }
                   
                }
            }

        }
        #endregion

        #region "Add File Notes"

        public void AddNotes(ref NotesModel objNotesModel)
        {
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "FileNotes_Add";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@description", objNotesModel.Description);
                sqlParams[1] = new SqlParameter("@text", objNotesModel.Text);
                sqlParams[2] = new SqlParameter("@fileId", objNotesModel.FileId);
                sqlParams[3] = new SqlParameter("@siteid", objNotesModel.SiteId);

                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);

                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }

        public void EditNote(ref NotesModel objNotesModel)
        {
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "FileNotes_Edit";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@description", objNotesModel.Description);
                sqlParams[1] = new SqlParameter("@text", objNotesModel.Text);
                sqlParams[2] = new SqlParameter("@Id", objNotesModel.Id);
                sqlParams[3] = new SqlParameter("@siteid", objNotesModel.SiteId);

                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);

                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }
        #endregion

        #region "CountHandoutViewsOrPrintsMonthwise"

        public int GetPrintCountHandoutViewsOrPrintsMonthwise(int siteid,int personid,int handoutid,DateTime date,int roleid,int viewtype,int languageid)
        {

            //Declaring List of objects type Person
            List<FolderFiles> lstfolderFiles = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                try
                {
                    string strSPName = "CountHandoutViewsOrPrintsMonthwise";
                    SqlParameter[] sqlParams = new SqlParameter[7];
                    sqlParams[0] = new SqlParameter("@SiteID", siteid);
                    sqlParams[1] = new SqlParameter("@PersonId", personid);
                    sqlParams[2] = new SqlParameter("@HandoutId", handoutid);
                    sqlParams[3] = new SqlParameter("@Date", date);
                    sqlParams[4] = new SqlParameter("@RoleId", roleid);
                    sqlParams[5] = new SqlParameter("@ViewType", viewtype);
                    sqlParams[6] = new SqlParameter("@LanguageId", languageid);

                    int printcount = (int)SqlHelper.ExecuteScalar(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);
                    sqlConn.Close();
                    return printcount;
                }
                finally
                {
                    
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        #endregion

        #region "Get All Printed Coversheets"

        public List<FileCoversheet> GetFileCoversheetsBySiteId(int siteid)
        {

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            List<FileCoversheet> lstCoverSheets = new List<FileCoversheet>();
            FileCoversheet objFileCoversheet;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Defining SQL Procedures
                    string strSPName = "FileCoversheets_GetForSite";
                    SqlParameter[] sqlParams = new SqlParameter[1];

                    sqlParams[0] = new SqlParameter("@siteid", siteid);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                   // this.FillNotes(ref objNotesModel, drData);

                    if(drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            objFileCoversheet = new FileCoversheet();

                            if (drData["Text"] != DBNull.Value)
                            {
                                objFileCoversheet.text = Convert.ToString(drData["Text"]);
                            }

                           
                            lstCoverSheets.Add(objFileCoversheet);
                        }
                    }


                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();

                    return lstCoverSheets;

                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        #endregion
    }
}