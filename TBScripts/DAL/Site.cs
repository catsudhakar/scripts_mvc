//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Site
    {
        public Site()
        {
            this.People = new HashSet<Person>();
            this.ProductLines = new HashSet<ProductLine>();
        }
    
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public int CustomerId { get; set; }
        public string SiteNumber { get; set; }
        public string BrandFooterFilePath { get; set; }
        public Nullable<int> BrandTypeId { get; set; }
        public string BrandHeaderFilePath { get; set; }
        public string ApplicationHeaderFilePath { get; set; }
        public bool SuppressOurBrand { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Notes { get; set; }
    
        public virtual BrandType BrandType { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual StateProvince StateProvince { get; set; }
        public virtual ICollection<ProductLine> ProductLines { get; set; }
    }
}
