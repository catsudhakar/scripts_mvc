//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class PublishResult
    {
        public int ResultId { get; set; }
        public int HandoutId { get; set; }
        public int LanguageId { get; set; }
        public string ErrorDesc { get; set; }
        public string ErrorCode { get; set; }
        public Nullable<bool> DidSucceed { get; set; }
    
        public virtual Handout Handout { get; set; }
    }
}
