//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Section
    {
        public Section()
        {
            this.Chapters = new HashSet<Chapter>();
        }
    
        public int SectionId { get; set; }
        public string SectionTitle { get; set; }
        public int BookId { get; set; }
        public Nullable<int> SortSeq { get; set; }
    
        public virtual Book Book { get; set; }
        public virtual ICollection<Chapter> Chapters { get; set; }
    }
}
