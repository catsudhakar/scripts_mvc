﻿
using System;
using System.Collections.Generic;
using System.Text;
using TBScripts.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace TBScripts.DAL
{
    public class TreeViewDal : BaseDAL
    {
        public List<TreeViewModel> GetTreeView()
        {

            //Declaring List of objects type Person
            List<TreeViewModel> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<TreeViewModel>();

                    //Defining SQL Procedures
                    string strSPName = "spNavTreeGet";
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@SelectedView", 2);
                    sqlParams[1] = new SqlParameter("@SiteId", 51);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    this.FillTreeView(ref lstTreeView, drData, 0);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        public List<TreeViewModel> GetTreeViewByBookId(int? intBookId)
        {
            //intBookId = 32;
            //Declaring List of objects type Person
            List<TreeViewModel> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<TreeViewModel>();

                    //Defining SQL Procedures
                    string strSPName = "spNavTreeGetByBookId";
                    SqlParameter[] sqlParams = new SqlParameter[3];
                    sqlParams[0] = new SqlParameter("@SelectedView", 2);
                    sqlParams[1] = new SqlParameter("@SiteId", 51);
                    sqlParams[2] = new SqlParameter("@BookId", intBookId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    this.FillTreeView(ref lstTreeView, drData, intBookId);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        public List<SectionChapters> GetChaptersBySectionID(int? SectionId)
        {

            List<SectionChapters> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<SectionChapters>();

                    //Defining SQL Procedures
                    string strSPName = "SP_ChaptersBySectionID";
                    SqlParameter[] sqlParams = new SqlParameter[1];
                    sqlParams[0] = new SqlParameter("@SectionId", SectionId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    //this.FillPreNotifyReport(ref lstTreeView, drData);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            SectionChapters SecChapter = new SectionChapters();

                            if (drData["SectionId"] != DBNull.Value)
                            {
                                SecChapter.SectionId = Convert.ToInt32(drData["SectionId"]);

                            }
                            if (drData["SectionTitle"] != DBNull.Value)
                            {
                                SecChapter.SectionTitle = Convert.ToString(drData["SectionTitle"]);
                            }

                            if (drData["ChapterId"] != DBNull.Value)
                            {
                                SecChapter.ChapterId = Convert.ToInt32(drData["ChapterId"]);
                            }
                            if (drData["ChapterTitle"] != DBNull.Value)
                            {
                                SecChapter.ChapterTitle = Convert.ToString(drData["ChapterTitle"]);
                            }
                            lstTreeView.Add(SecChapter);
                        }
                    }
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        public List<ChapterHandOuts> GetHandoutsByChapterID(int? ChapterId)
        {

            List<ChapterHandOuts> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<ChapterHandOuts>();

                    //Defining SQL Procedures
                    string strSPName = "SP_HandoutsByChapterID";
                    SqlParameter[] sqlParams = new SqlParameter[1];
                    sqlParams[0] = new SqlParameter("@ChapterId", ChapterId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    //this.FillPreNotifyReport(ref lstTreeView, drData);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            ChapterHandOuts ObjTreeView = new ChapterHandOuts();

                            if (drData["ChapterId"] != DBNull.Value)
                            {
                                ObjTreeView.ChapterId = Convert.ToInt32(drData["ChapterId"]);
                            }
                            //if (drData["ChapterTitle"] != DBNull.Value)
                            //{
                            //    ObjTreeView.ChapterTitle = Convert.ToString(drData["ChapterTitle"]);
                            //}

                            if (drData["HandoutId"] != DBNull.Value)
                            {
                                ObjTreeView.HandoutId = Convert.ToInt32(drData["HandoutId"]);
                            }
                            if (drData["HandoutTitle"] != DBNull.Value)
                            {
                                ObjTreeView.HandoutTitle = Convert.ToString(drData["HandoutTitle"]);
                            }

                            if (drData["LanguageId"] != DBNull.Value)
                            {
                                ObjTreeView.LanguageId = Convert.ToInt32(drData["LanguageId"]);
                            }
                            if (drData["LanguageName"] != DBNull.Value)
                            {
                                ObjTreeView.LanguageName = Convert.ToString(drData["LanguageName"]);
                            }
                            lstTreeView.Add(ObjTreeView);
                        }
                    }
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }
        #region "Reports"
        public List<PreNotifyListModel> GetPreNotifyReportByBookId(int? intBookId)
        {

            List<PreNotifyListModel> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<PreNotifyListModel>();

                    //Defining SQL Procedures
                    string strSPName = "sp_PreNotifySiteAdmins";
                    SqlParameter[] sqlParams = new SqlParameter[1];
                    //sqlParams[0] = new SqlParameter("@SelectedView", 2);
                    //sqlParams[1] = new SqlParameter("@SiteId", 51);
                    sqlParams[0] = new SqlParameter("@BookId", intBookId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    this.FillPreNotifyReport(ref lstTreeView, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        private void FillPreNotifyReport(ref List<PreNotifyListModel> lstTreeview, SqlDataReader drData)
        {

            if (drData.HasRows)
            {
                while (drData.Read())
                {
                    PreNotifyListModel ObjTreeView = new PreNotifyListModel();

                    if (drData["BookId"] != DBNull.Value)
                    {
                        ObjTreeView.BookId = Convert.ToInt32(drData["BookId"]);
                    }
                    if (drData["BookTitle"] != DBNull.Value)
                    {
                        ObjTreeView.BookTitle = Convert.ToString(drData["BookTitle"]);
                    }
                    if (drData["AnticipatedPublishDate"] != DBNull.Value)
                    {
                        ObjTreeView.AnticipatedPublishDate = Convert.ToString(drData["AnticipatedPublishDate"]);
                    }
                    if (drData["SectionId"] != DBNull.Value)
                    {
                        ObjTreeView.SectionId = Convert.ToInt32(drData["SectionId"]);
                        //get files 

                    }
                    if (drData["SectionTitle"] != DBNull.Value)
                    {
                        ObjTreeView.SectionTitle = Convert.ToString(drData["SectionTitle"]);
                    }

                    if (drData["ChapterId"] != DBNull.Value)
                    {
                        ObjTreeView.ChapterId = Convert.ToInt32(drData["ChapterId"]);
                    }
                    if (drData["ChapterTitle"] != DBNull.Value)
                    {
                        ObjTreeView.ChapterTitle = Convert.ToString(drData["ChapterTitle"]);
                    }

                    if (drData["HandoutId"] != DBNull.Value)
                    {
                        ObjTreeView.HandoutId = Convert.ToInt32(drData["HandoutId"]);
                    }
                    if (drData["HandoutTitle"] != DBNull.Value)
                    {
                        ObjTreeView.HandoutTitle = Convert.ToString(drData["HandoutTitle"]);
                    }

                    if (drData["LanguageId"] != DBNull.Value)
                    {
                        ObjTreeView.LanguageId = Convert.ToInt32(drData["LanguageId"]);
                    }
                    if (drData["LanguageName"] != DBNull.Value)
                    {
                        ObjTreeView.LanguageName = Convert.ToString(drData["LanguageName"]);
                    }

                    lstTreeview.Add(ObjTreeView);
                }
            }

        }

        public List<FileStatusModel> GetFileStausBySectionId(int? intSectionId)
        {

            List<FileStatusModel> lstFileStaus = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstFileStaus = new List<FileStatusModel>();

                    //Defining SQL Procedures
                    string strSPName = "Files_GetAllForReport1";
                    SqlParameter[] sqlParams = new SqlParameter[1];
                    sqlParams[0] = new SqlParameter("@sectionId", intSectionId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    // this.FillPreNotifyReport(ref lstTreeView, drData);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            FileStatusModel ObjFileStatus = new FileStatusModel();

                            if (drData["Id"] != DBNull.Value)
                            {
                                ObjFileStatus.Id = Convert.ToInt32(drData["Id"]);
                            }
                            if (drData["Name"] != DBNull.Value)
                            {
                                ObjFileStatus.Name = Convert.ToString(drData["Name"]);
                            }
                            if (drData["Status"] != DBNull.Value)
                            {
                                ObjFileStatus.status = Convert.ToInt32(drData["Status"]);
                            }
                            lstFileStaus.Add(ObjFileStatus);
                        }
                    }
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstFileStaus;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }


        private void FillTreeView(ref List<TreeViewModel> lstTreeview, SqlDataReader drData, int? BookId)
        {

            if (drData.HasRows)
            {
                while (drData.Read())
                {
                    TreeViewModel ObjTreeView = new TreeViewModel();
                    if (drData["SiteID"] != DBNull.Value)
                    {
                        ObjTreeView.intSiteId = Convert.ToInt32(drData["SiteID"]);
                    }
                    if (drData["ProductLineId"] != DBNull.Value)
                    {
                        ObjTreeView.intProductLineId = Convert.ToInt32(drData["ProductLineId"]);
                    }
                    if (drData["ProductLineDesc"] != DBNull.Value)
                    {
                        ObjTreeView.strProductLineDesc = Convert.ToString(drData["ProductLineDesc"]);
                    }
                    if (drData["SectionId"] != DBNull.Value)
                    {
                        ObjTreeView.intSectionId = Convert.ToInt32(drData["SectionId"]);
                    }
                    if (drData["SectionTitle"] != DBNull.Value)
                    {
                        ObjTreeView.strSectionTitle = Convert.ToString(drData["SectionTitle"]);
                    }
                    if (BookId > 0)
                    {
                        if (ObjTreeView.intSectionId > 0)
                        {
                            ObjTreeView.lstHandOutModel = AddFiles(ObjTreeView.intSectionId);
                        }
                    }
                    if (drData["BookId"] != DBNull.Value)
                    {
                        ObjTreeView.intBookId = Convert.ToInt32(drData["BookId"]);
                    }
                    if (drData["BookTitle"] != DBNull.Value)
                    {
                        ObjTreeView.strBookTitle = Convert.ToString(drData["BookTitle"]);
                    }
                    lstTreeview.Add(ObjTreeView);
                }
            }

        }

        private void FillHandouts(ref List<HandoutModel> lstHandouts, SqlDataReader drData, int sessionId)
        {

            if (drData.HasRows)
            {
                while (drData.Read())
                {
                    HandoutModel ObjHandout = new HandoutModel();
                    ObjHandout.intsectionId = sessionId;
                    if (drData["Id"] != DBNull.Value)
                    {
                        ObjHandout.intHandOutId = Convert.ToInt32(drData["Id"]);
                    }
                    if (drData["Name"] != DBNull.Value)
                    {
                        ObjHandout.strName = Convert.ToString(drData["Name"]);
                    }
                    if (drData["keywords"] != DBNull.Value)
                    {
                        ObjHandout.strkeywords = Convert.ToString(drData["keywords"]);
                    }

                    if (drData["statusName"] != DBNull.Value)
                    {
                        ObjHandout.strstatusName = Convert.ToString(drData["statusName"]);
                    }

                    ObjHandout.strHtmlstring = "<NOBR>" + "<a class=ob_a href='Files/ViewFrame.aspx?id=" + ObjHandout.intHandOutId + "' target='PrimaryAppFrame'>" + ObjHandout.strName + "</a></NOBR>";


                    lstHandouts.Add(ObjHandout);

                }
            }

        }

        public List<HandoutCounts> GetTotalUsageReport(int? SiteId, int? LanguageId, string StartDate, string EndDate)
        {

            List<HandoutCounts> lstTreeView = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstTreeView = new List<HandoutCounts>();

                    //Defining SQL Procedures
                    string strSPName = "stprGetSysAdminReports";
                    SqlParameter[] sqlParams = new SqlParameter[5];
                   
                    sqlParams[0] = new SqlParameter("@SiteId", SiteId);
                    sqlParams[1] = new SqlParameter("@LanguageId", LanguageId);
                    sqlParams[2] = new SqlParameter("@StartDate", StartDate);
                    sqlParams[3] = new SqlParameter("@EndDate", EndDate);
                    sqlParams[4] = new SqlParameter("@AceessStatus", 0);


                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            HandoutCounts ObjHandout = new HandoutCounts();

                            if (drData["Id"] != DBNull.Value)
                            {
                                ObjHandout.HandoutId = Convert.ToInt32(drData["HandoutId"]);
                            }
                            if (drData["Name"] != DBNull.Value)
                            {
                                ObjHandout.HandoutTitle = Convert.ToString(drData["NavigationTitle"]);
                            }
                            if (drData["keywords"] != DBNull.Value)
                            {
                                ObjHandout.ViewsCount = Convert.ToInt32(drData["Views"]);
                            }

                            if (drData["statusName"] != DBNull.Value)
                            {
                                ObjHandout.PrintCounts = Convert.ToInt32(drData["Prints"]);
                            }




                            lstTreeView.Add(ObjHandout);
                        }
                    }
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstTreeView;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }
        #endregion



        public List<HandoutModel> AddFiles(int sessionId)
        {

            //Declaring List of objects type Person
            List<HandoutModel> lstHandouts = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstHandouts = new List<HandoutModel>();

                    //Defining SQL Procedures
                    string strSPName = string.Empty;
                    //if (m_lRoleId == SITE_ADMIN_ROLE | m_lRoleId == SYSTEM_ADMIN_ROLE | m_lRoleId == AUTHOR_ROLE)
                    strSPName = "Files_GetAll";
                    // else
                    //strSPName = "Files_GetAllPublished";
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@sectionId", sessionId);
                    sqlParams[1] = new SqlParameter("@languageId", 1);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    this.FillHandouts(ref lstHandouts, drData, sessionId);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstHandouts;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        //private void AddFiles(Int32 SectionId)
        //{
        //    System.Data.SqlClient.SqlCommand com = null;
        //    if (m_lRoleId == SITE_ADMIN_ROLE | m_lRoleId == SYSTEM_ADMIN_ROLE | m_lRoleId == AUTHOR_ROLE)
        //    {
        //        com = SqlHelper.e("Files_GetAll");
        //    }
        //    else
        //    {
        //        com = Helper.DB.ProcedureCommand("Files_GetAllPublished");
        //    }
        //    com.Parameters.Add("@sectionId", SectionId);
        //    Int32 languageId = default(Int32);
        //    languageId = 1;
        //    //Convert.ToInt32(HttpContext.Current.Session("SelectedLanguageId"))
        //    com.Parameters.Add("@languageId", languageId);
        //    com.CommandTimeout = "10000";

        //    System.Data.SqlClient.SqlDataReader r = null;
        //    r = com.ExecuteReader();

        //    Int32 id = default(Int32);
        //    string name = null;

        //    while ((r.Read()))
        //    {
        //        id = r.GetInt32(0);
        //        name = r.GetString(1);

        //        string sHTML = null;
        //        sHTML = "<NOBR>" + "<a class=ob_a href='Files/ViewFrame.aspx?id=" + id + "' target='PrimaryAppFrame'>" + name + "</a></NOBR>";
        //        m_oTree.Add("sc_" + SectionId, "fs_" + id, sHTML, false, "database.gif");

        //    }
        //    r.Close();
        //    com.Connection.Close();
        //}


        public List<LanguageModel> GetLanguages()
        {

            List<LanguageModel> lstLanguages = null;
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {
                SqlDataReader drData = null;
                try
                {
                    lstLanguages = new List<LanguageModel>();
                    string strSPName = "select LanguageId,languageName from language";
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.Text, strSPName);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            LanguageModel ObjLanguage = new LanguageModel();
                            if (drData["LanguageId"] != DBNull.Value)
                            {
                                ObjLanguage.intlanguageId = Convert.ToInt32(drData["LanguageId"]);
                            }
                            if (drData["languageName"] != DBNull.Value)
                            {
                                ObjLanguage.strlanguageName = Convert.ToString(drData["languageName"]);
                            }
                            lstLanguages.Add(ObjLanguage);
                        }
                    }
                    drData.Close();
                    sqlConn.Close();
                    return lstLanguages;
                }
                finally
                {
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<ProductLineTypeModel> GetProductLineType()
        {

            List<ProductLineTypeModel> lstProductLineType = null;
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {
                SqlDataReader drData = null;
                try
                {
                    lstProductLineType = new List<ProductLineTypeModel>();
                    string strSPName = "select ProductLineTypeId,ProductLineTypeDesc from ProductLineType";
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.Text, strSPName);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            ProductLineTypeModel ObjProductLineType = new ProductLineTypeModel();
                            if (drData["ProductLineTypeId"] != DBNull.Value)
                            {
                                ObjProductLineType.intProductLineTypeId = Convert.ToInt32(drData["ProductLineTypeId"]);
                            }
                            if (drData["ProductLineTypeDesc"] != DBNull.Value)
                            {
                                ObjProductLineType.strProductLineTypeDesc = Convert.ToString(drData["ProductLineTypeDesc"]);
                            }
                            lstProductLineType.Add(ObjProductLineType);
                        }
                    }
                    drData.Close();
                    sqlConn.Close();
                    return lstProductLineType;
                }
                finally
                {
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<ddlProductLineTypeModel> GetProductLineTypeForDDl()
        {

            List<ddlProductLineTypeModel> lstProductLineType = null;
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {
                SqlDataReader drData = null;
                try
                {
                    lstProductLineType = new List<ddlProductLineTypeModel>();
                    string strSPName = "select ProductLineTypeId,ProductLineTypeDesc from ProductLineType";
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.Text, strSPName);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            ddlProductLineTypeModel ObjProductLineType = new ddlProductLineTypeModel();
                            if (drData["ProductLineTypeId"] != DBNull.Value)
                            {
                                ObjProductLineType.intProductLineTypeId = Convert.ToInt32(drData["ProductLineTypeId"]);
                            }
                            if (drData["ProductLineTypeDesc"] != DBNull.Value)
                            {
                                ObjProductLineType.strProductLineTypeDesc = Convert.ToString(drData["ProductLineTypeDesc"]);
                            }
                            lstProductLineType.Add(ObjProductLineType);
                        }
                    }
                    drData.Close();
                    sqlConn.Close();
                    return lstProductLineType;
                }
                finally
                {
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<ProductLineModel> GetProductLine()
        {

            List<ProductLineModel> lstProductLineType = null;
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            try
            {
                SqlDataReader drData = null;
                try
                {
                    lstProductLineType = new List<ProductLineModel>();
                    string strSPName = "SELECT ProductLineId,ProductLineDesc,ProductLineTypeId,CopyrightDesc FROM ProductLine";
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.Text, strSPName);
                    if (drData.HasRows)
                    {
                        while (drData.Read())
                        {
                            ProductLineModel ObjProductLineType = new ProductLineModel();
                            if (drData["ProductLineId"] != DBNull.Value)
                            {
                                ObjProductLineType.ProductLineId = Convert.ToInt32(drData["ProductLineId"]);
                            }
                            if (drData["ProductLineDesc"] != DBNull.Value)
                            {
                                ObjProductLineType.ProductLineDesc = Convert.ToString(drData["ProductLineDesc"]);
                            }
                            lstProductLineType.Add(ObjProductLineType);
                        }
                    }
                    drData.Close();
                    sqlConn.Close();
                    return lstProductLineType;
                }
                finally
                {
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void GetProductLine(ref ProductModel objproduct)
        {

            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {

                    //Defining SQL Procedures
                    string strSPName = "Get_Selected_ProductLine";
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@ProductLineId", objproduct.intProductLineId);
                    sqlParams[1] = new SqlParameter("@LanguageId", objproduct.intlanguageId);

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName, sqlParams);

                    //Filling PersonList from drData
                    if (drData.HasRows)
                    {
                        while ((drData.Read()))
                        {

                            if (drData["ProductLineTypeId"] != DBNull.Value)
                            {
                                objproduct.intProductLineTypeId = Convert.ToInt32(drData["ProductLineTypeId"]);
                            }

                            if (drData["LanguageId"] != DBNull.Value)
                            {
                                objproduct.intlanguageId = Convert.ToInt32(drData["LanguageId"]);
                            }

                            if (drData["ProductLineDesc"] != DBNull.Value)
                            {
                                objproduct.strProductLineDesc = Convert.ToString(drData["ProductLineDesc"]);
                            }

                            if (drData["CopyrightDesc"] != DBNull.Value)
                            {
                                objproduct.CopyrightDesc = Convert.ToString(drData["CopyrightDesc"]);
                            }

                            if (drData["Disclaimer"] != DBNull.Value)
                            {
                                objproduct.strDisclaimer = Convert.ToString(drData["Disclaimer"]);
                            }
                        }
                    }
                    drData.Close();
                    sqlConn.Close();
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
        }

        #region SetProductLine
        ///<Summary>SetProductLine - Used for Insert Or Update ProductLine
        /// <param name="objProductLine"></param>
        ///</Summary>
        public void SetProductLine(ref ProductModel objProductLine)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "usp_InsertProductLineDetails";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                SqlParameter[] sqlParams = this.CreateSqlParameters(ref objProductLine);
                //Executing SQl Query
                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);
                //If it is new record then update the productLineId with return value
                if (objProductLine.intProductLineId == 0)
                {
                    objProductLine.intProductLineId = Convert.ToInt32(sqlParams[0].Value);
                }
                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }

        private SqlParameter[] CreateSqlParameters(ref ProductModel objProductLine)
        {
            SqlParameter[] sqlParams = new SqlParameter[4];
            sqlParams[0] = new SqlParameter("@ProductLineId", objProductLine.intProductLineId);
            sqlParams[0].Direction = ParameterDirection.InputOutput;
            sqlParams[1] = new SqlParameter("@ProductLineDesc", objProductLine.strProductLineDesc);
            sqlParams[2] = new SqlParameter("@ProductLineTypeId", objProductLine.intProductLineTypeId);
            sqlParams[3] = new SqlParameter("@CopyRightDesc", objProductLine.CopyrightDesc);
            return sqlParams;
        }

        public void UpdateProductLine(ref ProductModel objProductLine)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "Update_ProductLine";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                // SqlParameter[] sqlParams = this.CreateSqlParameters(ref objProductLine);

                SqlParameter[] sqlParams = new SqlParameter[6];
                sqlParams[0] = new SqlParameter("@ProductLineId", objProductLine.intProductLineId);
                // sqlParams[0].Direction = ParameterDirection.InputOutput;
                sqlParams[1] = new SqlParameter("@ProductLineDesc", objProductLine.strProductLineDesc);
                sqlParams[2] = new SqlParameter("@ProductLineType", objProductLine.intProductLineTypeId);
                sqlParams[3] = new SqlParameter("@ProductLineCopyrightDesc", objProductLine.CopyrightDesc);
                sqlParams[4] = new SqlParameter("@Disclaimer", objProductLine.strDisclaimer);
                sqlParams[5] = new SqlParameter("@LanguageId", objProductLine.intlanguageId);

                //Executing SQl Query
                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);
                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }
        #endregion

        #region "Books"

        public void SetBooks(ref BookModel objBook)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            //Declaring object of type SqlTransaction
            SqlTransaction sqlTran = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Defining Stored Procedure
                string strSPName = "usp_InsertBookDetails";

                //Begin SQL Transaction
                sqlTran = sqlConn.BeginTransaction();
                //Creating SQL Parameters array
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@BookTitle", objBook.strBookTitle);
                sqlParams[1] = new SqlParameter("@BookTypeId", objBook.intBookTypeId);
                sqlParams[2] = new SqlParameter("@ProductLineId", objBook.intProductLineId);

                SqlHelper.ExecuteNonQuery(sqlTran, strSPName, sqlParams);

                //Commit SQL Transaction
                sqlTran.Commit();
                //Closing SQL Connection
                sqlConn.Close();
            }
            catch (Exception ex)
            {
                if (sqlTran != null) // Checking whether sqlTran is not Null
                    sqlTran.Rollback(); //Rollback SQL Transaction
                //Throwing Exception to Business Layer 
                throw ex;
            }
            finally
            {
                //Disposing all objects
                if (sqlConn != null)
                {
                    sqlConn.Dispose();
                }
            }
        }



        public void GetBookByBookId(ref BookModel objBook)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Defining SQL Procedures
                    string strSPName = "usp_GetBookDetailsByBookId";
                    SqlParameter sqlParam = new SqlParameter("@BookId", objBook.intBookId);

                    //Executing Stored Procedure 
                    drData = SqlHelper.ExecuteReader(sqlConn, strSPName, sqlParam);

                    //Filling Data from drData
                    this.FillBook(ref objBook, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) //Checking whether drData is not null or not
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) //Checking whether sqlConn is not null or not
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
        }

        private void FillBook(ref BookModel objBookModel, SqlDataReader drData)
        {

            if (drData.HasRows)
            {
                if (drData.Read())
                {
                    if (drData["BookId"] != DBNull.Value)
                    {
                        objBookModel.intBookId = Convert.ToInt32(drData["BookId"]);
                    }

                    if (drData["BookTitle"] != DBNull.Value)
                    {
                        objBookModel.strBookTitle = Convert.ToString(drData["BookTitle"]);
                    }
                    if (drData["ProductLineId"] != DBNull.Value)
                    {
                        objBookModel.intProductLineId = Convert.ToInt32(drData["ProductLineId"]);
                    }

                    if (drData["BookTypeId"] != DBNull.Value)
                    {
                        objBookModel.intBookTypeId = Convert.ToInt32(drData["BookTypeId"]);
                    }

                    if (drData["ChangesDeadline"] != DBNull.Value)
                    {
                        objBookModel.ChangesDeadline = Convert.ToDateTime(drData["ChangesDeadline"]);
                    }

                    if (drData["AnticipatedPublishDate"] != DBNull.Value)
                    {
                        objBookModel.AnticipatedPublishDate = Convert.ToDateTime(drData["AnticipatedPublishDate"]);
                    }

                    if (drData["NotificationSentDate"] != DBNull.Value)
                    {
                        objBookModel.NotificationSentDate = Convert.ToDateTime(drData["NotificationSentDate"]);
                    }

                    if (drData["DatePublished"] != DBNull.Value)
                    {
                        objBookModel.ChangesDeadline = Convert.ToDateTime(drData["DatePublished"]);
                    }


                }
            }

        }
        #endregion

        #region "sesctions"
        public List<SectionModel> getSectionsByBookId(int bookId)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;
            List<SectionModel> lstSections = new List<SectionModel>();

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Defining SQL Procedures
                    string strSPName = "usp_getSectionsByBookId";
                    SqlParameter sqlParam = new SqlParameter("@BookId", bookId);

                    //Executing Stored Procedure 
                    drData = SqlHelper.ExecuteReader(sqlConn, strSPName, sqlParam);

                    //Filling Data from drData
                    this.FillSections(ref lstSections, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    return lstSections;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) //Checking whether drData is not null or not
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) //Checking whether sqlConn is not null or not
                    {
                        sqlConn.Dispose();
                    }

                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
        }

        private void FillSections(ref List<SectionModel> lstSection, SqlDataReader drData)
        {

            if (drData.HasRows)
            {
                SectionModel objBookModel = null;
                while (drData.Read())
                {
                    objBookModel = new SectionModel();
                    if (drData["BookId"] != DBNull.Value)
                    {
                        objBookModel.BookId = Convert.ToInt32(drData["BookId"]);
                    }
                    if (drData["BookTitle"] != DBNull.Value)
                    {
                        objBookModel.BookTitle = Convert.ToString(drData["BookTitle"]);
                    }
                    if (drData["SortSeq"] != DBNull.Value)
                    {
                        objBookModel.SortSequence = Convert.ToInt32(drData["SortSeq"]);
                    }
                    if (drData["SectionId"] != DBNull.Value)
                    {
                        objBookModel.SectionId = Convert.ToInt32(drData["SectionId"]);
                    }

                    if (drData["SectionTitle"] != DBNull.Value)
                    {
                        objBookModel.SectionTitle = Convert.ToString(drData["SectionTitle"]);
                    }
                    lstSection.Add(objBookModel);
                }
            }
        }
        #endregion
    }
}



