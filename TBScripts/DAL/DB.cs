﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System;
using System.Data;
using System.Data.SqlClient;

using System.Collections;

namespace TBScripts.DAL
{
    public class DB
    {


        public static SqlCommand ProcedureCommand(string name)
        {

            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["PHdataConnectionString2"].ToString();

            //SqlConnection c = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"]);

            SqlConnection c = new SqlConnection(conString);
            c.Open();


            SqlCommand com = new SqlCommand(name, c);
            com.CommandType = System.Data.CommandType.StoredProcedure;
            return com;
        }


        public static object ProcedureScalar(string name, Hashtable parameters)
        {
            SqlCommand com = ProcedureCommand(name);

            foreach (string n in parameters.Keys)
            {
                com.Parameters.Add(n, parameters[n]);
            }
            object r = com.ExecuteScalar();
            com.Connection.Close();
            return r;
        }

        public static System.Data.DataTable ProcedureTable(string name, Hashtable parameters)
        {

            SqlCommand com = ProcedureCommand(name);

            if (parameters != null)
                foreach (string n in parameters.Keys)
                {
                    com.Parameters.Add(n, parameters[n]);
                }

            System.Data.SqlClient.SqlDataReader reader = com.ExecuteReader();


            System.Data.DataTable table = new DataTable();
            int count = reader.FieldCount;
            for (int i = 0; i < count; i++)
            {
                table.Columns.Add(reader.GetName(i));
            }

            while (reader.Read())
            {
                DataRow row = table.NewRow();

                for (int i = 0; i < count; i++)
                {
                    row[i] = reader.GetValue(i);
                }
                table.Rows.Add(row);
            }

            com.Connection.Close();

            return table;
        }




        public static object ProcedureScalarForId(string name, int id)
        {
            SqlCommand com = ProcedureCommand(name);
            com.Parameters.Add("@id", id);
            object r = com.ExecuteScalar();
            com.Connection.Close();
            return r;
        }



        public static System.Data.DataTable ProcedureTable(string name, int id)
        {

            SqlCommand com = ProcedureCommand(name);
            com.Parameters.Add("@id", id);
            System.Data.SqlClient.SqlDataReader reader = com.ExecuteReader();


            System.Data.DataTable table = new DataTable();
            int count = reader.FieldCount;
            for (int i = 0; i < count; i++)
            {
                table.Columns.Add(reader.GetName(i));
            }

            while (reader.Read())
            {
                DataRow row = table.NewRow();

                for (int i = 0; i < count; i++)
                {
                    row[i] = reader.GetValue(i);
                }
                table.Rows.Add(row);
            }

            com.Connection.Close();

            return table;
        }

        public static System.Web.UI.WebControls.ListItemCollection ProcedureListItemCollection(string name, int id)
        {

            SqlCommand com = ProcedureCommand(name);
            com.Parameters.Add("@id", id);
            System.Data.SqlClient.SqlDataReader reader = com.ExecuteReader();

            System.Web.UI.WebControls.ListItemCollection li = new System.Web.UI.WebControls.ListItemCollection();

            while (reader.Read())
            {
                int lid = reader.GetInt32(0);
                string lname = reader.GetString(1);
                li.Add(new System.Web.UI.WebControls.ListItem(lname, lid.ToString()));
            }

            com.Connection.Close();

            return li;
        }

    }
}
