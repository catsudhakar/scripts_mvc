﻿
using System;
using System.Collections.Generic;
using System.Text;
using TBScripts.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace TBScripts.DAL
{
    public class PersonDalc : BaseDAL
    {
        public List<PersonModel> GetPersonList()
        {

            //Declaring List of objects type Person
            List<PersonModel> lstPerson = null;

            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Creating instance of List of objects type Person
                    lstPerson = new List<PersonModel>();

                    //Defining SQL Procedures
                    string strSPName = "usp_GetListPerson";

                    //Executing Stored Procedure
                    drData = SqlHelper.ExecuteReader(sqlConn, System.Data.CommandType.StoredProcedure, strSPName);

                    //Filling PersonList from drData
                    this.FillPersonList(ref lstPerson, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                    ////Returns List<Person>
                    return lstPerson;
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) // Checking whether drData is not Null
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) // Checking whether sqlConn is not Null
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }

        }

        #region FillPerson List
        ///<Summary>FillPersonList - Used for Fill all the List data
        /// <param name="ref lstPerson"></param>
        /// <param name="drData"></param>
        ///</Summary>

        private void FillPersonList(ref List<PersonModel> lstPerson, SqlDataReader drData)
        {

            //Checking whether drData has records or not
            if (drData.HasRows) // if dtData has records 
            {
                //Navigating to all dtData records to fill data
                while (drData.Read())
                {
                    PersonModel objPerson = new PersonModel();
                    if (drData["PersonId"] != DBNull.Value)
                    {
                        objPerson.personId = Convert.ToInt32(drData["PersonId"]);
                    }
                    if (drData["FirstName"] != DBNull.Value)
                    {
                        objPerson.firstName = Convert.ToString(drData["FirstName"]);
                    }
                    if (drData["LastName"] != DBNull.Value)
                    {
                        objPerson.lastName = Convert.ToString(drData["LastName"]);
                    }
                    if (drData["SiteId"] != DBNull.Value)
                    {
                        objPerson.siteId = Convert.ToInt32(drData["SiteId"]);
                    }
                    if (drData["EmailAddress"] != DBNull.Value)
                    {
                        objPerson.emailAddress = Convert.ToString(drData["EmailAddress"]);
                    }
                    if (drData["LoginId"] != DBNull.Value)
                    {
                        objPerson.loginId = Convert.ToString(drData["LoginId"]);
                    }
                    if (drData["Password"] != DBNull.Value)
                    {
                        objPerson.password = Convert.ToString(drData["Password"]);
                    }
                    if (drData["RoleId"] != DBNull.Value)
                    {
                        objPerson.roleId = Convert.ToInt32(drData["RoleId"]);
                    }
                    if (drData["IsActive"] != DBNull.Value)
                    {
                        objPerson.isActive = Convert.ToBoolean(drData["IsActive"]);
                    }
                    //if (drData["Phone1TypeId"] != DBNull.Value)
                    //{
                    //    objPerson.Phone1TypeId = Convert.ToInt32(drData["Phone1TypeId"]);
                    //}
                    //if (drData["Phone2TypeId"] != DBNull.Value)
                    //{
                    //    objPerson.Phone2TypeId = Convert.ToInt32(drData["Phone2TypeId"]);
                    //}
                    //if (drData["Phone3TypeId"] != DBNull.Value)
                    //{
                    //    objPerson.Phone3TypeId = Convert.ToInt32(drData["Phone3TypeId"]);
                    //}
                    //if (drData["Phone4TypeId"] != DBNull.Value)
                    //{
                    //    objPerson.Phone4TypeId = Convert.ToInt32(drData["Phone4TypeId"]);
                    //}
                    //if (drData["PhoneNumber1"] != DBNull.Value)
                    //{
                    //    objPerson.PhoneNumber1 = Convert.ToString(drData["PhoneNumber1"]);
                    //}
                    //if (drData["PhoneNumber2"] != DBNull.Value)
                    //{
                    //    objPerson.PhoneNumber2 = Convert.ToString(drData["PhoneNumber2"]);
                    //}
                    //if (drData["PhoneNumber3"] != DBNull.Value)
                    //{
                    //    objPerson.PhoneNumber3 = Convert.ToString(drData["PhoneNumber3"]);
                    //}
                    //if (drData["PhoneNumber4"] != DBNull.Value)
                    //{
                    //    objPerson.PhoneNumber4 = Convert.ToString(drData["PhoneNumber4"]);
                    //}
                    //if (drData["Notes"] != DBNull.Value)
                    //{
                    //    objPerson.Notes = Convert.ToString(drData["Notes"]);
                    //}
                    //Adding Person to the BE Object
                    lstPerson.Add(objPerson);
                }
            }

        }

        #endregion

        #region GetPerson
        ///<Summary>GetPerson - Used for retrieving particular item List data
        /// <param name="objPerson"></param>
        ///</Summary>

        public void GetPerson(ref PersonModel objPerson)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Defining SQL Procedures
                    string strSPName = "usp_GetPerson";
                    SqlParameter sqlParam = new SqlParameter("@PersonId", objPerson.personId);

                    //Executing Stored Procedure 
                    drData = SqlHelper.ExecuteReader(sqlConn, strSPName, sqlParam);

                    //Filling Data from drData
                    this.FillPerson(ref objPerson, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) //Checking whether drData is not null or not
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) //Checking whether sqlConn is not null or not
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
        }
        #endregion
        public void GetPersonByLoginId(ref PersonModel objPerson)
        {
            //Declaring object of type SqlConnection
            SqlConnection sqlConn = null;

            try
            {
                //Getting instance of type SqlConnection from BASEDALC
                sqlConn = this.GetConnection();
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
            try
            {
                //Declaring object of type SqlDataReader
                SqlDataReader drData = null;
                try
                {
                    //Defining SQL Procedures
                    string strSPName = "usp_GetPersonDetailsByLoginId";
                    SqlParameter[] sqlParams = new SqlParameter[2];
                    sqlParams[0] = new SqlParameter("@LoginId", objPerson.loginId);
                    sqlParams[1] = new SqlParameter("@password", objPerson.password);
                    
                    //Executing Stored Procedure 
                    drData = SqlHelper.ExecuteReader(sqlConn, strSPName, sqlParams);

                    //Filling Data from drData
                    this.FillPerson(ref objPerson, drData);
                    //Closing SqlDatareader object
                    drData.Close();
                    //Closing SqlConnection object
                    sqlConn.Close();
                }
                finally
                {
                    //Disposing all objects
                    if (drData != null) //Checking whether drData is not null or not
                    {
                        ((IDisposable)drData).Dispose();
                    }
                    if (sqlConn != null) //Checking whether sqlConn is not null or not
                    {
                        sqlConn.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                //Throwing Exception to Business Layer 
                throw ex;
            }
        }
        private void FillPerson(ref PersonModel objPerson, SqlDataReader drData)
        {

            if (drData.HasRows)
            {
                if (drData.Read())
                {
                    if (drData["PersonId"] != DBNull.Value)
                    {
                        objPerson.personId = Convert.ToInt32(drData["PersonId"]);
                    }
                    if (drData["FirstName"] != DBNull.Value)
                    {
                        objPerson.firstName = Convert.ToString(drData["FirstName"]);
                    }
                    if (drData["LastName"] != DBNull.Value)
                    {
                        objPerson.lastName = Convert.ToString(drData["LastName"]);
                    }
                    if (drData["SiteId"] != DBNull.Value)
                    {
                        objPerson.siteId = Convert.ToInt32(drData["SiteId"]);
                    }
                    if (drData["EmailAddress"] != DBNull.Value)
                    {
                        objPerson.emailAddress = Convert.ToString(drData["EmailAddress"]);
                    }
                    if (drData["LoginId"] != DBNull.Value)
                    {
                        objPerson.loginId = Convert.ToString(drData["LoginId"]);
                    }
                    if (drData["Password"] != DBNull.Value)
                    {
                        objPerson.password = Convert.ToString(drData["Password"]);
                    }
                    if (drData["RoleId"] != DBNull.Value)
                    {
                        objPerson.roleId = Convert.ToInt32(drData["RoleId"]);
                    }
                    if (drData["IsActive"] != DBNull.Value)
                    {
                        objPerson.isActive = Convert.ToBoolean(drData["IsActive"]);
                    }
                    if (drData["Phone1TypeId"] != DBNull.Value)
                    {
                        objPerson.phone1TypeId = Convert.ToInt32(drData["Phone1TypeId"]);
                    }
                    if (drData["Phone2TypeId"] != DBNull.Value)
                    {
                        objPerson.phone2TypeId = Convert.ToInt32(drData["Phone2TypeId"]);
                    }
                    if (drData["Phone3TypeId"] != DBNull.Value)
                    {
                        objPerson.phone3TypeId = Convert.ToInt32(drData["Phone3TypeId"]);
                    }
                    if (drData["Phone4TypeId"] != DBNull.Value)
                    {
                        objPerson.phone4TypeId = Convert.ToInt32(drData["Phone4TypeId"]);
                    }
                    if (drData["PhoneNumber1"] != DBNull.Value)
                    {
                        objPerson.phoneNumber1 = Convert.ToString(drData["PhoneNumber1"]);
                    }
                    if (drData["PhoneNumber2"] != DBNull.Value)
                    {
                        objPerson.phoneNumber2 = Convert.ToString(drData["PhoneNumber2"]);
                    }
                    if (drData["PhoneNumber3"] != DBNull.Value)
                    {
                        objPerson.phoneNumber3 = Convert.ToString(drData["PhoneNumber3"]);
                    }
                    if (drData["PhoneNumber4"] != DBNull.Value)
                    {
                        objPerson.phoneNumber4 = Convert.ToString(drData["PhoneNumber4"]);
                    }
                    if (drData["Notes"] != DBNull.Value)
                    {
                        objPerson.notes = Convert.ToString(drData["Notes"]);
                    }
                    if (drData["SiteName"] != DBNull.Value)
                    {
                        objPerson.siteName = Convert.ToString(drData["SiteName"]);
                    }

                    if (drData["RoleName"] != DBNull.Value)
                    {
                        objPerson.roleName = Convert.ToString(drData["RoleName"]);
                    }
                }
            }

        }

    }
}