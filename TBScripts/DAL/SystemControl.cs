//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBScripts.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemControl
    {
        public int SystemControlId { get; set; }
        public string SystemLockedReason { get; set; }
        public string LostPasswordResponse { get; set; }
        public string WelcomeAnnouncement { get; set; }
        public string StagingDirectoryPath { get; set; }
        public string ProductionDirectoryPath { get; set; }
        public string ArchiveDirectoryPath { get; set; }
        public string ArtDirectoryPath { get; set; }
        public string TreeRootTitle { get; set; }
    }
}
