﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Exception;
using Microsoft.VisualBasic;
using VBCollection = Microsoft.VisualBasic.Collection;

namespace TBScripts.DAL
{
    public class clsSQLReader
    {
  
    
    private const string CS_APP_NAME = "SparkV2";
    
    private const string CS_CLASS_NAME = "SparkV2.clsSQLReader";
    
    private string m_sErr = "";
    
    private string m_sLongErr = "";
    
    private string m_sSource = "";
    
    private SqlConnection m_oConn = new SqlConnection();
    
    private string m_sConnStrKey = "";
    
    private string m_sConnStr = "";

    
    
    private VBCollection  m_coParms = new VBCollection();
    
    public string Error_Desc {
        get {
            return m_sErr;
        }
    }
    
    public string Error_LongDesc {
        get {
            return m_sLongErr;
        }
    }
    
    public string Error_Source {
        get {
            return m_sSource;
        }
    }
    
    private void Set_Error(string sSource, string sErr, string sLongErr) {
        string sError = "";
        System.Diagnostics.EventLog oLog;
        oLog=new System.Diagnostics.EventLog();
       
        if ((m_sErr == "")) {
            m_sErr = sErr;
            m_sLongErr = sLongErr;
            if ((sSource.Substring(0, 1) == ".")) {
                // If the first char is a period - meaning an internal method or sub was the source
                m_sSource = (CS_CLASS_NAME + sSource);
                // store error source
                sError = ("Error in " 
                            + (m_sSource + (": " + sLongErr)));
                // Prepare the string for Event report

              
                try {
                    if (!System.Diagnostics.EventLog.SourceExists(CS_APP_NAME)) {
                        System.Diagnostics.EventLog.CreateEventSource(CS_APP_NAME, "Application");
                    }
                    System.Diagnostics.EventLog.WriteEntry(CS_APP_NAME, sError, System.Diagnostics.EventLogEntryType.Error);
                }
                catch (Exception ex) {
                    // Throw ex
                }
            }
            else {
                m_sSource = sSource;
            }
        }
    }
    
    private struct MySQLParm {
        
        private string Name;
        
        private object Value;
    }
    
    public string ConnectionStringKey {
        get {
            return m_sConnStrKey;
        }
        set {
            m_sConnStrKey = value;
        }
    }
    
    public string ConnectionString {
        get {
            return m_sConnStr;
        }
        set {
            m_sConnStr = value;
        }
    }
    
    private string ConnStr() {
        ConnStr = "";
        if ((m_sConnStr.Trim() == "")) {
            try {
                if ((m_sConnStrKey.Trim() == "")) {
                    throw new NullReferenceException("No ConnectionString information has been provided.");
                }
                else if ((System.Configuration.ConfigurationSettings.AppSettings(m_sConnStrKey) == null)) {
                    throw new NullReferenceException(("ConnectionString configuration is missing from you web.config. It should contain  <connectionStrings>" +
                        " <add key=\"" 
                                    + (m_sConnStrKey + "\" value=\"Connection String Here\"> </connectionStrings>")));
                }
                else {
                    ConnStr = System.Configuration.ConfigurationSettings.AppSettings(m_sConnStrKey).ToString;
                }
            }
            catch (Exception ex) {
                Set_Error(".ConnStr", ex.Message, ex.ToString);
            }
        }
        else {
            ConnStr = m_sConnStr;
        }
    }
    
    private void AddParamToSQLCmd(SqlCommand oCmd, string sName, object oParamValue) {
        SqlParameter oParm = new SqlParameter();
        try {
            if ((oCmd == null)) {
                throw new ArgumentNullException("oCmd Is Nothing");
            }
            if ((sName == String.Empty)) {
                throw new ArgumentOutOfRangeException("sParamName Is Empty");
            }
            oParm.ParameterName = sName;
            oParm.Direction = ParameterDirection.Input;
            if (!(oParamValue == null)) {
                switch (TypeName(oParamValue)) {
                    case "Long":
                        oParm.DbType = DbType.Int64;
                        break;
                    case "Integer":
                        oParm.DbType = DbType.Int16;
                        break;
                    case "String":
                        oParm.DbType = DbType.String;
                        oParm.Size = oParamValue.Length;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("Typename Not Defined.");
                        break;
                }
                oParm.Value = oParamValue;
            }
            else {
                throw new ArgumentNullException("oParamValue is Nothing");
            }
            oCmd.Parameters.Add(oParm);
        }
        catch (Exception ex) {
            Set_Error(".AddParamToSQLCmd", ex.Message, ex.ToString);
        }
        finally {
            oParm = null;
        }
    }
    
    private SqlConnection Get_SQLConn(string sDB, void =, void ) {
        SqlConnection oConn = new SqlConnection();
        // Warning!!! Optional parameters not supported
        Get_SQLConn = null;
        oConn.ConnectionString = ConnStr();
        try {
            oConn.Open();
            if ((oConn.State == ConnectionState.Open)) {
                Get_SQLConn = oConn;
            }
        }
        catch (SqlException exSQL) {
            Set_Error(".Get_SQLConn", exSQL.Message, exSQL.ToString());
        }
        catch (Exception ex) {
            Set_Error(".Get_SQLConn", ex.Message, ex.ToString());
        }
    }
    
    private SqlCommand Get_SQLCmd(string sCmd) {
        SqlCommand oCmd = new SqlCommand();
        Get_SQLCmd = null;
        oCmd.CommandType = CommandType.StoredProcedure;
        oCmd.CommandTimeout = 90;
        oCmd.CommandText = sCmd;
        return oCmd;
    }
    
    public SqlDataReader ExecuteReader(string sSP) {
        SqlCommand oCmd = new SqlCommand();
        SqlDataReader oRead;
        int iLoop;
        MySQLParm oParm;
        ExecuteReader = null;
        try {
            oCmd = Get_SQLCmd(sSP);
            for (iLoop = 1; (iLoop <= m_coParms.Count); iLoop++) {
                oParm = m_coParms.Item[iLoop];
                AddParamToSQLCmd(oCmd, oParm.Name, oParm.Value);
            }
            m_oConn = Get_SQLConn();
            if (!(m_oConn == null)) {
                oCmd.Connection = m_oConn;
                if ((m_sErr == "")) {
                    oRead = oCmd.ExecuteReader;
                    ExecuteReader = oRead;
                }
            }
        }
        catch (Exception ex) {
            Set_Error(".ExecuteReader", ex.Message, ex.ToString());
        }
        finally {
            if ((TypeName(oCmd) == "SqlCommand")) {
                oCmd.Dispose();
            }
            oCmd = null;
        }
    }
    
    public void Dispose() {
        if ((TypeName(m_coParms) == "Collection")) {
            while ((m_coParms.Count > 0)) {
                m_coParms.Remove(1);
            }
        }
        m_coParms = null;
        if ((TypeName(m_oConn) == "SqlConnection")) {
            if ((m_oConn.State == ConnectionState.Open)) {
                m_oConn.Close();
            }
            m_oConn.Dispose();
        }
        m_oConn = null;
    }
    
    public void Add_Parm(string sParmName, object sParmVal) {
        MySQLParm oParm;
        oParm.Name = sParmName;
        oParm.Value = sParmVal;
        m_coParms.Add(oParm);
    }
    
    protected override void Finalize() {
        base.Finalize();
        this.Dispose();
    }
}
}