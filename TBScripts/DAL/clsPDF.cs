﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.VisualBasic;

using System.Collections;

using System.Data;
using System.Diagnostics;
using System.IO;
//using WebSupergoo.ABCpdf4;

//using WebSupergoo.ABCpdf4;
using WebSupergoo.ABCpdf10;

namespace TBScripts.DAL
{

    public class clsPDF
    {


        Doc m_oPDFDocument;
        public Doc PDFDocument
        {
            get { return m_oPDFDocument; }
            set { m_oPDFDocument = value; }
        }

        public int Page
        {
            get { return this.PDFDocument.Page; }
            set { this.PDFDocument.Page = value; }
        }

        public clsPDF()
        {
            //Initialize the Document
            m_oPDFDocument = new Doc();

            //Set the license key
//            m_oPDFDocument.SetInfo(0, "License", "528-298-844-117-4959-384");
            m_oPDFDocument.SetInfo(0, "License", "X/VKS0cNn5FhpydaFvHWOdS1fqY2HcFg+d7EyPXrYiUHxlzNdiK9NpNufzUohzgyfoV19T9PtFpFlbIgS7V53lIjaGGVDXg3zj4VcqiYOX7Mr0eCwe03SJL3LHyZVC0TQao7asdb9Qs1KH9ToSXPHFZYZi2QQ3++6qUc5xICW+fLiJM+oTvuqjqEip1m5FxraBFArkMW4iK/BRN7DONtiTNolW3dGuZiOLOX");


        }

        public void Close()
        {
            //Kills the objects in the class

            m_oPDFDocument = null;

        }

        public void AppendPage(string sURL)
        {

            Doc oDoc = default(Doc);

            oDoc = new Doc();
            //Set the license key
            //oDoc.SetInfo(0, "License", "528-298-844-117-4959-384");
            oDoc.SetInfo(0, "License", "X/VKS0cNn5FhpydaFvHWOdS1fqY2HcFg+d7EyPXrYiUHxlzNdiK9NpNufzUohzgyfoV19T9PtFpFlbIgS7V53lIjaGGVDXg3zj4VcqiYOX7Mr0eCwe03SJL3LHyZVC0TQao7asdb9Qs1KH9ToSXPHFZYZi2QQ3++6qUc5xICW+fLiJM+oTvuqjqEip1m5FxraBFArkMW4iK/BRN7DONtiTNolW3dGuZiOLOX");

            string s = HttpContext.Current.Server.MapPath(sURL).ToString();

            oDoc.Read(HttpContext.Current.Server.MapPath(sURL).ToString());
            //oDoc.Read("E:\\SudhakarRepo\\Scripts_MVC_02142017\\TBScripts\\Blank_pdf.pdf");
            
            this.PDFDocument.Append(oDoc);

        }

        public int AddURL(string URL)
        {
            int functionReturnValue = 0;
            long lRandomNumber = 0;
            try
            {

                //lRandomNumber = DateTime.(DateInterval.Second, DateAndTime.Today, DateAndTime.Now);
                Random r = new Random();

                lRandomNumber = r.Next();

                string url2 = null;
                url2 = URL + "&dummy=" + lRandomNumber;
                // Helper.Log.Save("Addurl");
                functionReturnValue = this.PDFDocument.AddImageUrl(url2, true, 0, true);
                //Helper.Log.Save(functionReturnValue.ToString());
                //Helper.Log.Save("Addimageurl");
            }
            catch (Exception ex)
            {
                //Helper.Log.Save(ex.Message);
                throw ex;
            }
            return functionReturnValue;

        }

        public int AddImageToChain(int id)
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.AddImageToChain(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;

        }

        public int AddText(string sText)
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.AddText(sText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;
        }

        public int AddHTML(string sText)
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.AddHtml(sText);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;
        }

        public int AddPage(int PageNumber)
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.AddPage(PageNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;
        }

        public int AddPage()
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.AddPage();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;
        }

        public void Clear()
        {
            this.PDFDocument.Clear();
        }

        public int Compress()
        {
            int functionReturnValue = 0;
            try
            {
                functionReturnValue = this.PDFDocument.Flatten();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return functionReturnValue;

        }

        public void InsetRect(double x, double y)
        {
            this.PDFDocument.Rect.Inset(x, y);
        }
        public void SetRect(double x, double y, double w, double h)
        {
            this.PDFDocument.Rect.SetRect(x, y, w, h);
        }

        public int FrameRect()
        {
            return this.PDFDocument.FrameRect();
        }

        public string GetInfo(int id, string type)
        {
            return this.PDFDocument.GetInfo(id, type);
        }

        public void Save(string sPath)
        {
            this.PDFDocument.Save(sPath);
        }

        public int PageCount()
        {
            return this.PDFDocument.PageCount;

        }

        public int PageNumber
        {
            get { return this.PDFDocument.PageNumber; }
            set { this.PDFDocument.PageNumber = value; }
        }

        public double RectWidth
        {
            get { return this.PDFDocument.Rect.Width; }
            set { this.PDFDocument.Rect.Width = value; }
        }

        public double RectHeight
        {
            get { return this.PDFDocument.Rect.Height; }
            set { this.PDFDocument.Rect.Height = value; }
        }

        public int AddImageObject(ref WebSupergoo.ABCpdf10.XImage image, bool transparent)
        {
            return this.PDFDocument.AddImageObject(image, transparent);
        }
    }
}