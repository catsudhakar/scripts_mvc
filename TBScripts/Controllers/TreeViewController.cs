﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.Models;
using TBScripts.DAL;

namespace TBScripts.Controllers
{
    public class TreeViewController : BaseController
    {
        private TreeViewDal objTreeViewDal;
        private TreeViewModel objTreeView;

        //
        // GET: /TreeView/
        public ActionResult Treeview()
        {
            int i = UDTO.siteId;
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetTreeView();
            return View(lstTreeView);
        }

        public ActionResult AddToEfolder(int id,string name)
        {

            Mymodel model = new Mymodel();
            model.Id = id;
            model.Name =HttpUtility.UrlDecode(name);
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetTreeView();
            model.treeViewModel = lstTreeView;

            if (lstTreeView.Count > 0)
            {
                ProductModel objProductModel = new ProductModel();
                objProductModel.intProductLineId = lstTreeView[0].intProductLineId;
                objProductModel.intlanguageId = 1;
                objTreeViewDal.GetProductLine(ref objProductModel);
                model.ProductModel = objProductModel;
            }
            //getting languages 
            List<LanguageModel> lstLanguage = new List<LanguageModel>();
            lstLanguage = objTreeViewDal.GetLanguages();
            ViewData["Language"] = lstLanguage;

            List<ProductLineTypeModel> lstProductLineType = new List<ProductLineTypeModel>();
            lstProductLineType = objTreeViewDal.GetProductLineType();
            ViewData["ProductLineType"] = lstProductLineType;

            return View(model);

        }

       //[CustomAuthorizeAttribute("Admin")]
        public ActionResult Browse()
        {

            //Dim dt As Data.DataTable
            //dt = Helper.DB.ProcedureTable("FillLanguage", 0)

            Mymodel model = new Mymodel();
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetTreeView();
            model.treeViewModel = lstTreeView;

            if (lstTreeView.Count > 0)
            {
                ProductModel objProductModel = new ProductModel();
                objProductModel.intProductLineId = lstTreeView[0].intProductLineId;
                objProductModel.intlanguageId = 1;
                objTreeViewDal.GetProductLine(ref objProductModel);

                //objProductModel.strProductLineDesc = "Teaching Book";
                //objProductModel.intProductLineTypeId = 2;
                //objProductModel.strProductLineTypeDesc = "3-Tier(no chapters)";
                //objProductModel.CopyrightDesc = "2007";

                model.ProductModel = objProductModel;
            }
            //getting languages 
            List<LanguageModel> lstLanguage = new List<LanguageModel>();
            lstLanguage = objTreeViewDal.GetLanguages();
            ViewData["Language"] = lstLanguage;
            //List<SelectListItem> li = new List<SelectListItem>();
            //li.Add(new SelectListItem { Text = "Select", Value = "0" });
            //  ViewData["Language"] = new SelectList(lstLanguage, "intlanguageId", "strlanguageName");
            // ViewData["Language"] = lstLanguage;

            //getting languages 
            List<ProductLineTypeModel> lstProductLineType = new List<ProductLineTypeModel>();
            lstProductLineType = objTreeViewDal.GetProductLineType();
            // ViewData["ProductLineType"] = new SelectList(lstProductLineType, "intProductLineTypeId", "strProductLineTypeDesc");
            ViewData["ProductLineType"] = lstProductLineType;

           // TempData["BooksItems"] = lstTreeView;


            return View(model);
        }

        public ActionResult Browse2(int? lintBookId)
        {
            Mymodel model = new Mymodel();
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetTreeViewByBookId(lintBookId);
            model.treeViewModel = lstTreeView;

            //BookModel objBook = new BookModel();
            //objBook.intBookId = (int)lintBookId;
            //objTreeViewDal.GetBookByBookId(ref objBook);
            //ViewData["Book"] = objBook;

            //getting Product Line 
            //List<ProductLineModel> lstProductLine = new List<ProductLineModel>();
            //lstProductLine = objTreeViewDal.GetProductLine();
            //ViewData["ProductLine"] = lstProductLine;


           // Mymodel model1 = new Mymodel();
            TreeViewModel objTreeView1 = new TreeViewModel();
            List<TreeViewModel> lstTreeView1 = new List<TreeViewModel>();
            lstTreeView1 = objTreeViewDal.GetTreeView();
           // model1.treeViewModel = lstTreeView1;
            //needs write loop here

            int lintPrvBookId = 0;
            List<TreeViewModel> t = new List<TreeViewModel>();
            foreach (var book in lstTreeView1)
            {
                if (book.intBookId != lintPrvBookId)
                {
                    t.Add(book);
                }
                lintPrvBookId = book.intBookId;
            }

            TempData["BooksItems"] = t;

            return View(model);
        }

        [ValidateInput(false)]
        public JsonResult Browse3(int? lintBookId)
        {
            Mymodel model = new Mymodel();
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetTreeViewByBookId(lintBookId);
            model.treeViewModel = lstTreeView;
            // return View(model);
            // var id = Ifbp.InsertingFormFields(formId, formFields);

            //BookModel objBook = new BookModel();
            //objBook.intBookId = (int)lintBookId;
            //objTreeViewDal.GetBookByBookId(ref objBook);
            //ViewData["Book"] = objBook;

            //ViewData["sudha"] = "sudhakar";

            ////getting Product Line 
            //List<ProductLineModel> lstProductLine = new List<ProductLineModel>();
            //lstProductLine = objTreeViewDal.GetProductLine();
            //ViewData["ProductLine"] = lstProductLine;


            // Mymodel model1 = new Mymodel();
            TreeViewModel objTreeView1 = new TreeViewModel();
            List<TreeViewModel> lstTreeView1 = new List<TreeViewModel>();
            lstTreeView1 = objTreeViewDal.GetTreeView();
            // model1.treeViewModel = lstTreeView1;
            //needs write loop here

            int lintPrvBookId = 0;
            List<TreeViewModel> t = new List<TreeViewModel>();
            foreach (var book in lstTreeView1)
            {
                if (book.intBookId != lintPrvBookId)
                {
                    t.Add(book);
                }
                lintPrvBookId = book.intBookId;
            }

            TempData["BooksItems"] = t;

            return Json(lstTreeView, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCountries()
        {

            List<SelectListItem> li = new List<SelectListItem>();

            li.Add(new SelectListItem { Text = "Select", Value = "0" });

            li.Add(new SelectListItem { Text = "India", Value = "1" });

            li.Add(new SelectListItem { Text = "Srilanka", Value = "2" });

            li.Add(new SelectListItem { Text = "China", Value = "3" });

            li.Add(new SelectListItem { Text = "Austrila", Value = "4" });

            li.Add(new SelectListItem { Text = "USA", Value = "5" });

            li.Add(new SelectListItem { Text = "UK", Value = "6" });

            ViewData["country"] = li;

            return View();
        }


       
    }
}