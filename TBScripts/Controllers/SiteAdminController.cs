﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.DAL;
using TBScripts.Models;

namespace TBScripts.Controllers
{
    public class SiteAdminController : BaseController
    {
        // GET: SiteAdmin
        public ActionResult Index()
        {

            if (TempData["TempEmpMessage"] != null)
            {
                ViewBag.EmpMessage = "User added sucessfully";
                TempData.Remove("TempEmpMessage");
            }

            return View();
        }

        public ActionResult Index1()
        {
            return View();
        }

        public PartialViewResult SiteAdminIndex()
        {
            return PartialView("_SiteAdminIndexPartial");
        }

        public PartialViewResult EditMySite(int siteId)
        {
            SiteModel model = new SiteModel();
            model = GetSiteBySiteId(siteId);
            return PartialView("_SiteAdminEditMySitePartial", model);
        }

        public ActionResult AddMySiteEmployee1(PersonModel model)
        {

            List<Site> lstSite = new List<Site>();
            List<PhoneType> lstPhoneType = new List<PhoneType>();
            List<Role> lstRole = new List<Role>();

            try
            {
                SavePerson(model);
                lstSite = GetSites();
                lstRole = GetRoles();
                lstPhoneType = GetPhoneType();

                model.lstSite = lstSite;
                model.lstPhoneType = lstPhoneType;
                model.lstRole = lstRole;

                TempData["TempEmpMessage"] = "User added sucessfully";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return PartialView("_SiteAdminEmployeePartial", model);
            //return RedirectToAction("Index");
            // return Json("Sucess");
        }

        public string SaveSiteEmployee(PersonModel model)
        {
            //return PartialView("_SiteAdminEmployeePartial", model);
            //return Json("Sucess", JsonRequestBehavior.AllowGet);
            return "sudhakar";
        }

        public int SavePerson(PersonModel model)
        {
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (model.personId > 0)
                    {
                        var dbPerson = DBContext.People.Where(location => location.PersonId == model.personId).SingleOrDefault();
                        if (dbPerson != null)
                        {

                            dbPerson.FirstName = model.firstName;
                            dbPerson.LastName = model.lastName;
                            dbPerson.SiteId = model.siteId;
                            dbPerson.LoginId = model.loginId;
                            dbPerson.Password = model.password;
                            dbPerson.IsActive = model.isActive;
                            dbPerson.RoleId = model.roleId;
                            dbPerson.EmailAddress = model.emailAddress;
                            dbPerson.Notes = model.notes;
                            dbPerson.Phone1TypeId = model.phone1TypeId;
                            dbPerson.Phone2TypeId = model.phone2TypeId;
                            dbPerson.Phone3TypeId = model.phone3TypeId;
                            dbPerson.Phone4TypeId = model.phone4TypeId;

                            dbPerson.PhoneNumber1 = model.phoneNumber1;
                            dbPerson.PhoneNumber2 = model.phoneNumber2;
                            dbPerson.PhoneNumber3 = model.phoneNumber3;
                            dbPerson.PhoneNumber4 = model.phoneNumber4;

                            DBContext.Entry(dbPerson).State = System.Data.Entity.EntityState.Modified;
                            return DBContext.SaveChanges();

                        }
                    }
                    else
                    {
                        TBScripts.DAL.Person AddCust = new TBScripts.DAL.Person();
                        AddCust.PersonId = model.personId;
                        AddCust.FirstName = model.firstName;
                        AddCust.LastName = model.lastName;
                        AddCust.SiteId = model.siteId;
                        AddCust.LoginId = model.loginId;
                        AddCust.Password = model.password;
                        AddCust.Notes = model.notes;
                        AddCust.RoleId = model.roleId;
                        AddCust.EmailAddress = model.emailAddress;
                        AddCust.IsActive = model.isActive;
                        AddCust.Phone1TypeId = model.phone1TypeId;
                        AddCust.Phone2TypeId = model.phone2TypeId;
                        AddCust.Phone3TypeId = model.phone3TypeId;
                        AddCust.Phone4TypeId = model.phone4TypeId;

                        AddCust.PhoneNumber1 = model.phoneNumber1;
                        AddCust.PhoneNumber2 = model.phoneNumber2;
                        AddCust.PhoneNumber3 = model.phoneNumber3;
                        AddCust.PhoneNumber4 = model.phoneNumber4;


                        DBContext.People.Add(AddCust);
                        return DBContext.SaveChanges();
                    }

                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;

            }
            return 0;
        }

        public PartialViewResult AddMySiteEmployee(int siteId, int PersonId)
        {
            PersonModel model = new PersonModel();
            List<Site> lstSite = new List<Site>();
            List<PhoneType> lstPhoneType = new List<PhoneType>();
            List<Role> lstRole = new List<Role>();

            try
            {
                lstSite = GetSites();
                lstRole = GetRoles();
                lstPhoneType = GetPhoneType();

                if (PersonId > 0)
                {
                    model = GetPersonByPersonId(PersonId);
                }
                model.lstSite = lstSite;
                model.lstPhoneType = lstPhoneType;
                model.lstRole = lstRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return PartialView("_SiteAdminEmployeePartial", model);
        }

        public PartialViewResult ViewMySiteEmployee()
        {
            int siteId = 10;
            List<PersonModel> lstPersonModel = new List<PersonModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbPerson = from p in DBContext.People
                                   join s in DBContext.Sites on p.SiteId equals s.SiteId
                                   join r in DBContext.Roles on p.RoleId equals r.RoleId
                                   where s.IsActive == true && p.SiteId == siteId
                                   select p;
                    foreach (var item in dbPerson)
                    {
                        PersonModel Persn = new PersonModel();
                        Persn.personId = item.PersonId;
                        Persn.firstName = item.FirstName;
                        Persn.lastName = item.LastName;
                        Persn.siteName = item.Site.SiteName;
                        Persn.emailAddress = item.EmailAddress;
                        Persn.loginId = item.LoginId;
                        Persn.isActive = item.IsActive;
                        Persn.roleName = item.Role.RoleName;

                        lstPersonModel.Add(Persn);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return PartialView("_SiteAdminViewEmployeePartial", lstPersonModel);
        }

        public PartialViewResult MySiteEmployeeReport(int siteId)
        {

            List<PersonModel> lstPersonModel = new List<PersonModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbPerson = from p in DBContext.People
                                   join s in DBContext.Sites on p.SiteId equals s.SiteId
                                   join r in DBContext.Roles on p.RoleId equals r.RoleId
                                   where s.IsActive == true && p.SiteId == siteId
                                   select p;
                    foreach (var item in dbPerson)
                    {
                        PersonModel Persn = new PersonModel();
                        Persn.personId = item.PersonId;
                        Persn.firstName = item.FirstName;
                        Persn.lastName = item.LastName;
                        Persn.siteName = item.Site.SiteName;
                        Persn.emailAddress = item.EmailAddress;
                        Persn.loginId = item.LoginId;
                        Persn.isActive = item.IsActive;
                        Persn.roleName = item.Role.RoleName;

                        lstPersonModel.Add(Persn);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return PartialView("_SiteAdminViewEmployeePartial", lstPersonModel);
        }

        public SiteModel GetSiteBySiteId(int SiteId)
        {
            SiteModel model = new SiteModel();
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            List<BrandTypeModel> lstBrandTypeModel = new List<BrandTypeModel>();
            List<StateModel> lstStateModel = new List<StateModel>();

            if (SiteId > 0)
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    var dbsite = DBContext.Sites.Where(s => s.SiteId == SiteId).SingleOrDefault();
                    if (dbsite != null)
                    {
                        model.SiteId = dbsite.SiteId;
                        model.SiteName = dbsite.SiteName;
                        model.SiteNumber = dbsite.SiteNumber;
                        model.CustomerId = dbsite.CustomerId;
                        model.FaxNumber = dbsite.FaxNumber;
                        model.PhoneNumber = dbsite.PhoneNumber;
                        model.Address1 = dbsite.Address1;
                        model.Address2 = dbsite.Address2;
                        model.Address3 = dbsite.Address3;
                        model.StateId = dbsite.StateId;
                        model.City = dbsite.City;
                        model.PostalCode = dbsite.PostalCode;
                        model.IsActive = (bool)dbsite.IsActive;
                        model.Notes = dbsite.Notes;
                        model.BrandTypeId = dbsite.BrandTypeId;
                        model.SuppressOurBrand = dbsite.SuppressOurBrand;
                        model.BrandFooterFilePath = dbsite.BrandFooterFilePath;
                        model.BrandHeaderFilePath = dbsite.BrandHeaderFilePath;
                        model.ApplicationHeaderFilePath = dbsite.ApplicationHeaderFilePath;
                    }


                }
            }

            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbCustomers = DBContext.Customers.Where(s => s.IsActive == true).OrderBy(s => s.CustomerName).ToList();
                if (dbCustomers != null)
                {
                    foreach (var item in dbCustomers)
                    {
                        CustomerModel objCustomerModel = new CustomerModel();
                        objCustomerModel.CustomerId = item.CustomerId;
                        objCustomerModel.CustomerName = item.CustomerName;
                        lstCustomerModel.Add(objCustomerModel);
                    }
                }
            }
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbCustomers = DBContext.BrandTypes.ToList();
                if (dbCustomers != null)
                {
                    foreach (var item in dbCustomers)
                    {
                        BrandTypeModel objBrandTypeModel = new BrandTypeModel();
                        objBrandTypeModel.BrandTypeId = item.BrandTypeId;
                        objBrandTypeModel.BrandTypeDesc = item.BrandTypeDesc;
                        lstBrandTypeModel.Add(objBrandTypeModel);
                    }
                }
            }

            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {
                var dbstates = from s in DBContext.StateProvinces select s;
                foreach (var item in dbstates)
                {
                    StateModel prod = new StateModel();
                    prod.StateId = item.StateId;
                    prod.StateName = item.StateName;
                    lstStateModel.Add(prod);
                }
            }
            model.lstCustomer = lstCustomerModel;
            model.lstBrandType = lstBrandTypeModel;
            model.lstStateProvince = lstStateModel;


            return model;
        }
        [HttpPost]
        public ActionResult AddSite(SiteModel model, HttpPostedFileBase FileBrandFooterFilePath, HttpPostedFileBase FileBrandHeaderFilePath, HttpPostedFileBase FileApplicationHeaderFilePath)
        {

            string savefile = string.Empty;
            if (FileBrandFooterFilePath != null)
            {
                if (FileBrandFooterFilePath.FileName != null && FileBrandFooterFilePath.ContentLength > 0)
                {
                    if (FileBrandFooterFilePath.ContentType.ToLower() == "image/gif" || FileBrandFooterFilePath.ContentType.ToLower() == "image/jpg" || FileBrandFooterFilePath.ContentType.ToLower() == "image/jpeg" || FileBrandFooterFilePath.ContentType.ToLower() == "image/png")
                    {
                        model.BrandFooterFilePath = FileBrandFooterFilePath.FileName;
                        savefile = "~/SiteBrands/" + FileBrandFooterFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileBrandFooterFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }
            if (FileBrandHeaderFilePath != null)
            {
                if (FileBrandHeaderFilePath.FileName != null && FileBrandHeaderFilePath.ContentLength > 0)
                {
                    if (FileBrandHeaderFilePath.ContentType.ToLower() == "image/gif" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/jpg" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/jpeg" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/png")
                    {
                        model.BrandHeaderFilePath = FileBrandHeaderFilePath.FileName;
                        savefile = "~/SiteBrands/" + FileBrandHeaderFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileBrandHeaderFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }
            if (FileApplicationHeaderFilePath != null)
            {

                if (FileApplicationHeaderFilePath.FileName != null && FileApplicationHeaderFilePath.ContentLength > 0)
                {
                    if (FileApplicationHeaderFilePath.ContentType.ToLower() == "image/gif" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/jpg" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/jpeg" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/png")
                    {
                        savefile = "~/SiteBrands/" + FileApplicationHeaderFilePath.FileName;
                        model.ApplicationHeaderFilePath = FileApplicationHeaderFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileApplicationHeaderFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }

            //if (ModelState.IsValid)
            //{
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    //Update
                    TBScripts.DAL.Site AddCust = new TBScripts.DAL.Site();
                    var dblocations = DBContext.Sites.Where(s => s.SiteId == model.SiteId).FirstOrDefault();
                    dblocations.SiteName = model.SiteName;
                    dblocations.SiteNumber = model.SiteNumber;

                    dblocations.SiteNumber = model.SiteNumber;
                    dblocations.CustomerId = model.CustomerId;
                    dblocations.FaxNumber = model.FaxNumber;
                    dblocations.PhoneNumber = model.PhoneNumber;
                    dblocations.Address1 = model.Address1;
                    dblocations.Address2 = model.Address2;
                    dblocations.Address3 = model.Address3;
                    dblocations.StateId = model.StateId;
                    dblocations.City = model.City;
                    dblocations.PostalCode = model.PostalCode;
                    dblocations.IsActive = (bool)model.IsActive;
                    dblocations.Notes = model.Notes;
                    dblocations.BrandTypeId = model.BrandTypeId;
                    dblocations.SuppressOurBrand = model.SuppressOurBrand;
                    dblocations.BrandFooterFilePath = model.BrandFooterFilePath;
                    dblocations.BrandHeaderFilePath = model.BrandHeaderFilePath;
                    dblocations.ApplicationHeaderFilePath = model.ApplicationHeaderFilePath;

                    // DBContext.SaveChanges();


                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            //  }  //zmodel state 

            //var name = FileUpload1.FileName;
            // return Json("Sucess", JsonRequestBehavior.AllowGet);
            return RedirectToAction("EditSIte");
        }


        public List<Site> GetSites()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbsites = DBContext.Sites.Where(s => s.IsActive == true).OrderBy(s => s.SiteName).ToList();
                return dbsites;

            }
        }

        public List<Role> GetRoles()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbrole = DBContext.Roles.OrderBy(p => p.RoleName).ToList();
                return dbrole;

            }
        }

        public List<PhoneType> GetPhoneType()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbPhoneType = DBContext.PhoneTypes.OrderBy(p => p.PhoneTypeDesc).ToList();
                return dbPhoneType;

            }
        }

        public PersonModel GetPersonByPersonId(int PersonId)
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {


                var dbPerson = DBContext.People.Where(x => x.PersonId == PersonId).Select(x => new PersonModel
                {

                    firstName = x.FirstName,
                    lastName = x.LastName,
                    siteName = x.Site.SiteName,
                    emailAddress = x.EmailAddress,
                    loginId = x.LoginId,
                    isActive = x.IsActive,
                    roleName = x.Role.RoleName,
                    notes = x.Notes,
                    phone1TypeId = x.Phone1TypeId,
                    phone2TypeId = x.Phone2TypeId,
                    phone3TypeId = x.Phone3TypeId,
                    phone4TypeId = x.Phone4TypeId,
                    password = x.Password,
                    phoneNumber1 = x.PhoneNumber1,
                    phoneNumber2 = x.PhoneNumber2,
                    phoneNumber3 = x.PhoneNumber3,
                    phoneNumber4 = x.PhoneNumber4,
                    personId = x.PersonId,
                    siteId = x.SiteId,
                    roleId = x.RoleId,
                   

                }).SingleOrDefault();


                return dbPerson;

            }
        }

        //New Site Admin



        public ActionResult AddSiteUser(int PersonId)
        {
            PersonModel model = new PersonModel();
            if (Session["Person"] != null)
            {
                UDTO = (PersonModel)Session["Person"];
                if (UDTO.siteId > 0)
                {
                    model.siteId = UDTO.siteId;
                }
                else
                {

                }
            }

            List<Site> lstSite = new List<Site>();
            List<PhoneType> lstPhoneType = new List<PhoneType>();
            List<Role> lstRole = new List<Role>();

            try
            {
                lstSite = GetSites();
                lstRole = GetRoles();
                lstPhoneType = GetPhoneType();

                if (PersonId > 0)
                {
                    model = GetPersonByPersonId(PersonId);
                }
                model.lstSite = lstSite;
                model.lstPhoneType = lstPhoneType;
                model.lstRole = lstRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(model);
        }

        public ActionResult ViewUsers()
        {
            int siteId = 0;
            if (Session["Person"] != null)
            {
                UDTO = (PersonModel)Session["Person"];
                if (UDTO.siteId > 0)
                {
                    siteId = UDTO.siteId;
                }
                else
                {

                }
            }

            List<PersonModel> lstPersonModel = new List<PersonModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbPerson = from p in DBContext.People
                                   join s in DBContext.Sites on p.SiteId equals s.SiteId
                                   join r in DBContext.Roles on p.RoleId equals r.RoleId
                                   where s.IsActive == true && p.SiteId == siteId
                                   select p;
                    foreach (var item in dbPerson)
                    {
                        PersonModel Persn = new PersonModel();
                        Persn.personId = item.PersonId;
                        Persn.firstName = item.FirstName;
                        Persn.lastName = item.LastName;
                        Persn.siteName = item.Site.SiteName;
                        Persn.emailAddress = item.EmailAddress;
                        Persn.loginId = item.LoginId;
                        Persn.isActive = item.IsActive;
                        Persn.roleName = item.Role.RoleName;

                        lstPersonModel.Add(Persn);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return View(lstPersonModel);
        }

        public ActionResult EditSite()
        {
            int siteId = 0;
            if (Session["Person"] != null)
            {
                UDTO = (PersonModel)Session["Person"];
                if (UDTO.siteId > 0)
                {
                    siteId = UDTO.siteId;
                }
                else
                {

                }
            }
            SiteModel model = new SiteModel();
            model = GetSiteBySiteId(siteId);
            return View(model);
        }

        private List<ReportsModel> getReport()
        {
            var lstReports = new List<ReportsModel>();
            try
            {

                string conString = System.Configuration.ConfigurationManager.ConnectionStrings["PHdataConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conString);
                con.Open();

                SqlCommand com = new SqlCommand("stprGetReportsByMonthWiseNew", con);
                com.CommandType = System.Data.CommandType.StoredProcedure;

                var siteidParam = new SqlParameter("@SiteId", System.Data.SqlDbType.Int);
                var personidParam = new SqlParameter("@PersonId", System.Data.SqlDbType.Int);
                var typeParam = new SqlParameter("@ViewType", System.Data.SqlDbType.Int);
                var roleidParam = new SqlParameter("@RoleId", System.Data.SqlDbType.Int);
                var lanidParam = new SqlParameter("@LanguageId", System.Data.SqlDbType.Int);
                var yearParam = new SqlParameter("@FromYear", System.Data.SqlDbType.Int);
                siteidParam.Value = 164;
                personidParam.Value = 52;
                typeParam.Value = 2;
                roleidParam.Value = 1;
                lanidParam.Value = 1;
                yearParam.Value = 2016;

                com.Parameters.Add(siteidParam);
                com.Parameters.Add(personidParam);
                com.Parameters.Add(typeParam);
                com.Parameters.Add(roleidParam);
                com.Parameters.Add(lanidParam);
                com.Parameters.Add(yearParam);

                SqlDataReader dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ReportsModel r = new ReportsModel();
                        r.loginId = dr["LoginId"].ToString();
                        r.firstName = dr["FirstName"].ToString();
                        r.lastName = dr["LastName"].ToString();
                        r.Jan = dr["Jan"].ToString();
                        r.Feb = dr["Feb"].ToString();
                        r.March = dr["Mar"].ToString();
                        r.April = dr["Apr"].ToString();
                        r.May = dr["May"].ToString();
                        r.June = dr["Jun"].ToString();
                        r.July = dr["Jul"].ToString();
                        r.Aug = dr["Aug"].ToString();
                        r.Sep = dr["Sep"].ToString();
                        r.Oct = dr["Oct"].ToString();
                        r.Nov = dr["Nov"].ToString();
                        r.Dec = dr["Dec"].ToString();
                        r.ViewType = Convert.ToInt32(dr["ViewType"]);
                        r.LanguageId = Convert.ToInt32(dr["LanguageId"]);
                        lstReports.Add(r);
                    }
                }
                com.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }





            return lstReports;
        }

        public ActionResult SiteEmployeeUsage()
        {
            int siteId = 0;
            if (Session["Person"] != null)
            {
                UDTO = (PersonModel)Session["Person"];
                if (UDTO.siteId > 0)
                {
                    siteId = UDTO.siteId;
                }
                else
                {

                }
            }
            ReportsModel model = new ReportsModel();
            var lstReports = new List<ReportsModel>();
            lstReports = getReport();
            //using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            //{

            //    //var courseList = DBContext.stprGetReportsByMonthWiseNew(164, 52, 2, 1, 1, 2016).ToList<ReportsModel>();

            //    // var dbrole = DBContext.stprGetReportsByMonthWiseNew(164, 52, 2, 1, 1, 2016).tolist();
            //    // var dbrole = DBContext.stprGetReportsByMonthWiseNew(164, 52, 2, 1, 1, 2016);
            //    // return dbrole;

            //    //var idParam = new SqlParameter{terName = "StudentID", Value = 1};siteidParam,personidParam,typeParam,roleidParam,lanidParam,yearParam
            //    var siteidParam = new SqlParameter("@SiteId", System.Data.SqlDbType.Int);
            //    var personidParam = new SqlParameter("@PersonId", System.Data.SqlDbType.Int);
            //    var typeParam = new SqlParameter("@ViewType", System.Data.SqlDbType.Int);
            //    var roleidParam = new SqlParameter("@RoleId", System.Data.SqlDbType.Int);
            //    var lanidParam = new SqlParameter("@LanguageId", System.Data.SqlDbType.Int);
            //    var yearParam = new SqlParameter("@FromYear", System.Data.SqlDbType.Int);
            //    siteidParam.Value = 164;
            //    personidParam.Value = 52;
            //    typeParam.Value = 2;
            //    roleidParam.Value = 1;
            //    lanidParam.Value = 1;
            //    yearParam.Value = 2016;
            //    //Get student name of string type
            //    var courseList = DBContext.Database.SqlQuery<ReportsModel>
            //        ("exec stprGetReportsByMonthWiseNew @SiteId,@PersonId,@ViewType,@RoleId,@LanguageId,@FromYear", siteidParam, personidParam, typeParam, roleidParam, lanidParam, yearParam)
            //        .ToList<ReportsModel>();

            //   // DBContext.Database.("exec stprGetReportsByMonthWiseNew @SiteId,@PersonId,@ViewType,@RoleId,@LanguageId,@FromYear", siteidParam, personidParam, typeParam, roleidParam, lanidParam, yearParam);



            //}
            return View(lstReports);
        }


    }
}