﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TBScripts.DAL;
using TBScripts.Models;

namespace TBScripts.Controllers
{
    public class SystemAdminCodeTablesController : BaseController
    {


        // TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1();

        // GET: SystemAdminCodeTables
        public ActionResult ProductLineType()
        {
            return View();
        }

        public ActionResult ViewProductLineType()
        {

            List<ProductLineTypeModel> lstProductLineModel = new List<ProductLineTypeModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {


                    var dbProductLine = DBContext.ProductLineTypes.OrderBy(s => s.ProductLineTypeId).ToList();
                    if (dbProductLine != null)
                    {
                        foreach (var item in dbProductLine)
                        {
                            ProductLineTypeModel objSite = new ProductLineTypeModel();
                            objSite.intProductLineTypeId = item.ProductLineTypeId;
                            objSite.strProductLineTypeDesc = item.ProductLineTypeDesc;
                            objSite.blnHasChapters = (bool)item.HasChapters;
                            objSite.blnHasSections = (bool)item.HasSections;
                            lstProductLineModel.Add(objSite);
                        }
                    }


                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstProductLineModel);
        }

        public ActionResult ViewBrandType()
        {

            return RedirectToAction("Login", "Person");

            List<BrandTypeModel> lstProductLineModel = new List<BrandTypeModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {


                    var dbProductLine = DBContext.BrandTypes.OrderBy(s => s.BrandTypeDesc).ToList();
                    if (dbProductLine != null)
                    {
                        foreach (var item in dbProductLine)
                        {
                            BrandTypeModel objSite = new BrandTypeModel();
                            objSite.BrandTypeId = item.BrandTypeId;
                            objSite.BrandTypeDesc = item.BrandTypeDesc;

                            lstProductLineModel.Add(objSite);
                        }
                    }


                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstProductLineModel);
        }

        public ActionResult ViewRecentType()
        {
            return RedirectToAction("Login", "Person");
            //List<RecentTypeModel> lstProductLineModel = new List<RecentTypeModel>();
            //try
            //{


            //    using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            //    {


            //        var dbProductLine = DBContext.RecentTypes.OrderBy(s => s.RecentTypeDesc).ToList();
            //        if (dbProductLine != null)
            //        {
            //            foreach (var item in dbProductLine)
            //            {
            //                RecentTypeModel objSite = new RecentTypeModel();
            //                objSite.RecentTypeId = item.RecentTypeId;
            //                objSite.RecentTypeDesc = item.RecentTypeDesc;
            //                objSite.KeepCount = item.KeepCount;

            //                lstProductLineModel.Add(objSite);
            //            }
            //        }


            //    }

            //}

            //catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            //{
            //    Exception raise = dbEx;
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            string message = string.Format("{0}:{1}",
            //                validationErrors.Entry.Entity.ToString(),
            //                validationError.ErrorMessage);
            //            // raise a new exception nesting  
            //            // the current instance as InnerException  
            //            raise = new InvalidOperationException(message, raise);
            //        }
            //    }
            //    throw raise;
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}



            //return View(lstProductLineModel);
        }

        public ActionResult ViewPhoneType()
        {

            List<PhoneTypeModel> lstProductLineModel = new List<PhoneTypeModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {


                    var dbProductLine = DBContext.PhoneTypes.OrderBy(s => s.PhoneTypeDesc).ToList();
                    if (dbProductLine != null)
                    {
                        foreach (var item in dbProductLine)
                        {
                            PhoneTypeModel objSite = new PhoneTypeModel();
                            objSite.PhoneTypeId = item.PhoneTypeId;
                            objSite.PhoneTypeDesc = item.PhoneTypeDesc;
                            lstProductLineModel.Add(objSite);
                        }
                    }


                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstProductLineModel);
        }

        public ActionResult ViewLanguage()
        {

            List<LanguageModel> lstProductLineModel = new List<LanguageModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {


                    var dbProductLine = DBContext.Languages.OrderBy(s => s.LanguageName).ToList();
                    if (dbProductLine != null)
                    {
                        foreach (var item in dbProductLine)
                        {
                            LanguageModel objSite = new LanguageModel();
                            objSite.intlanguageId = item.LanguageId;
                            objSite.strlanguageName = item.LanguageName;
                            lstProductLineModel.Add(objSite);
                        }
                    }


                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstProductLineModel);
        }

        public ActionResult ViewStates()
        {

            List<StateModel> lstProductLineModel = new List<StateModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {


                    var dbProductLine = DBContext.StateProvinces.OrderBy(s => s.StateName).ToList();
                    if (dbProductLine != null)
                    {
                        foreach (var item in dbProductLine)
                        {
                            StateModel objSite = new StateModel();
                            objSite.StateId = item.StateId;
                            objSite.StateName = item.StateName;

                            lstProductLineModel.Add(objSite);
                        }
                    }


                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstProductLineModel);
        }
        [HttpPost]
        public JsonResult SaveProductLineType(string name, bool hassections, bool haschapters, string id)
        {
            try
            {
                int i = 0;
                if (!string.IsNullOrEmpty(id)) { i = Convert.ToInt32(id); }
                var dbProductLine = new ProductLineType();

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (i > 0)
                    {
                        dbProductLine = DBContext.ProductLineTypes.Where(s => s.ProductLineTypeId == i).FirstOrDefault();
                        if (dbProductLine != null)
                        {
                            dbProductLine.ProductLines=DBContext.ProductLines.Where(x=>x.ProductLineTypeId==i).ToList();
                            dbProductLine.ProductLineTypeDesc = name;
                            dbProductLine.HasSections = hassections;
                            dbProductLine.HasChapters = haschapters;
                            DBContext.Entry(dbProductLine).State = System.Data.Entity.EntityState.Modified;
                            DBContext.SaveChanges();
                        }
                    }
                    else
                    {
                        dbProductLine.ProductLineTypeDesc = name;
                        dbProductLine.HasSections = hassections;
                        dbProductLine.HasChapters = haschapters;
                        DBContext.ProductLineTypes.Add(dbProductLine);
                        DBContext.SaveChanges();
                    }

                }

                return Json(dbProductLine, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult DeleteProductLineType(int ProductLineTypeId)
        {
            int num = 0;
            try
            {
                if (ProductLineTypeId == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (ProductLineTypeId > 0)
                    {

                        var model = DBContext.ProductLineTypes.Where(x => x.ProductLineTypeId == ProductLineTypeId).FirstOrDefault();
                        DBContext.ProductLineTypes.Remove(model);
                        num = DBContext.SaveChanges();
                        
                    }
                }
                return Json(num);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}