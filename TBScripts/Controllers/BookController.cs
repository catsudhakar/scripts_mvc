﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.Models;
using TBScripts.DAL;

namespace TBScripts.Controllers
{
    public class BookController :  BaseController
    {

        private TreeViewDal objTreeViewDal;

        //
        // GET: /Book/
        public ActionResult AddBook(int ProdLineId, string Desc)
        {
            BookModel model = new BookModel();
            model.intProductLineId = ProdLineId;
            return View("AddBook", model);
        }

        public ActionResult AddBookDetails(string BookTitle, int BookType, int ProductLineId)
        {
            objTreeViewDal = new TreeViewDal();
            BookModel model = new BookModel();
            model.strBookTitle = BookTitle;
            model.intBookTypeId = BookType;
            model.intProductLineId = ProductLineId;

            objTreeViewDal.SetBooks(ref model);

            return View("AddBook", model);
        }
        //Edit Book
        public ActionResult EditBook(int lintBookId)
        {

            objTreeViewDal = new TreeViewDal();
            BookModel objBook = new BookModel();
            objBook.intBookId = (int)lintBookId;
            objTreeViewDal.GetBookByBookId(ref objBook);

            //getting Product Line 
            List<ProductLineModel> lstProductLine = new List<ProductLineModel>();
            lstProductLine = objTreeViewDal.GetProductLine();
            ViewData["ProductLine"] = lstProductLine;

            return View("EditBook", objBook);
        }
        //Getting List of Books
        public ActionResult BooksList()
        {
            return View();
        }

        public ActionResult UpdateBookDetails(int lintBookId, string BookName, DateTime? ChangesDeadline, DateTime? AnticipatedDate, DateTime? NotificationDate, DateTime? PublishedDate, int ProductLineId)
        {

            TreeViewDal objTreeViewDal = new TreeViewDal();
            BookModel objBook = new BookModel();
            objBook.intBookId = (int)lintBookId;
            objBook.strBookTitle = (string)BookName;
            objBook.ChangesDeadline = ChangesDeadline;
            objBook.AnticipatedPublishDate = AnticipatedDate;
            objBook.NotificationSentDate = NotificationDate;
            objBook.DatePublished = PublishedDate;
            objBook.intProductLineId = (int)ProductLineId;

            //Here needs write book update method
            objTreeViewDal.GetBookByBookId(ref objBook);

            //getting Product Line 
            List<ProductLineModel> lstProductLine = new List<ProductLineModel>();
            lstProductLine = objTreeViewDal.GetProductLine();
            ViewData["ProductLine"] = lstProductLine;

            return View("EditBook", objBook);
        }

        public ActionResult BookSections(int lintBookId)
        {

            objTreeViewDal = new TreeViewDal();
            SectionModel objSection = new SectionModel();
            List<SectionModel> lstSections = new List<SectionModel>();
            lstSections = objTreeViewDal.getSectionsByBookId(lintBookId);

            ////getting Product Line 
            //List<ProductLineModel> lstProductLine = new List<ProductLineModel>();
            //lstProductLine = objTreeViewDal.GetProductLine();
            //ViewData["ProductLine"] = lstProductLine;

            return View("BookSections", lstSections);
            //return View(lstSections);
        }

        public ActionResult AddSection(int? sectionid, string sectiontitle, int? sortseq)
        {
            SectionModel objSection = new SectionModel();
            objSection.SectionId = Convert.ToInt32(sectionid);
            if (sectiontitle != "" || sectiontitle != null)
                objSection.SectionTitle = sectiontitle.ToString();
            else
                objSection.SectionTitle = "";
            objSection.SortSequence = Convert.ToInt32(sortseq);
            return View("AddSection", objSection);
        }


        public ActionResult AddProductLine()
        {
            ProductModel model = new ProductModel();
            objTreeViewDal = new TreeViewDal();
            model.LstProductLineTypeModel = objTreeViewDal.GetProductLineTypeForDDl();
            return View("AddProductLine", model);
        }


        public ActionResult AddProductLine1(string ProductLineDesc, int ProductLineType, string Copyright)
        {
            objTreeViewDal = new TreeViewDal();
            ProductModel model = new ProductModel();
            model.strProductLineDesc = ProductLineDesc;
            model.intProductLineTypeId = ProductLineType;
            model.CopyrightDesc = Copyright;

            objTreeViewDal.SetProductLine(ref model);
            model.LstProductLineTypeModel = objTreeViewDal.GetProductLineTypeForDDl();
            if (model.intProductLineId > 0)
            {
                model.intProductLineTypeId = 0;
                model.strProductLineDesc = "";
                model.CopyrightDesc = "";
            }


            return View("AddProductLine", model);
        }


        [HttpPost]
        public ActionResult AddProductLine(ProductModel model)
        {
            objTreeViewDal = new TreeViewDal();
            objTreeViewDal.SetProductLine(ref model);
            model.LstProductLineTypeModel = objTreeViewDal.GetProductLineTypeForDDl();
            if (model.intProductLineId > 0)
            {
                model.intProductLineTypeId = 0;
                model.strProductLineDesc = "";
                model.CopyrightDesc = "";
            }


            return View("AddProductLine", model);
        }



        //    public ActionResult AddBookWithParam(int ProdLineId, string Desc)
        //  //  public ActionResult AddBookWithParam()    
        //{
        //        BookModel model = new BookModel();

        //        model.strBookTitle = "test mm";
        //        model.intBookId = 1;
        //        return View("AddBookWithParam",model);
        //    }
    }
}