﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.Models;
using TBScripts.DAL;
using System.IO;
using Microsoft.ApplicationBlocks.Data;
using Spire.License;
using System.Collections;
using WebSupergoo.ABCpdf10;



namespace TBScripts.Controllers
{
    public class FolderViewController : BaseController
    {
        PersonModel objPerson = null;
        eFolderModel objeFolderModel = null;
        List<eFolderModel> lsteFolders = null;
        eFolderDal objeFolderDal;
        //User Role Constants (corresponds to Role table)
        const long NURSE_ROLE = 1;
        const long SITE_ADMIN_ROLE = 2;
        const long PH_EMPLOYEE_ROLE = 3;
        const long SYSTEM_ADMIN_ROLE = 4;
        const long AUTHOR_ROLE = 5;
        const long REVIEWER_ROLE = 6;
        const long VISITOR_ROLE = 9;
        const long NURSE_ROLE_New = 10;

        // GET: /FolderView/
        [CustomAuthorizeAttribute(new string[] { "System Admin", "Admin" })]
        public ActionResult FolderView()
        {

            objPerson = new PersonModel();
            objeFolderModel = new eFolderModel();
            lsteFolders = new List<eFolderModel>();


            int m_lSiteId = 0;
            int m_lRoleId = 0;
            bool m_bShowEditLinks = false;
            string m_sRootNodeTile = string.Empty;
            if (Session["Person"] != null)
            {
                objPerson = (PersonModel)Session["Person"];
                if (string.IsNullOrEmpty(objPerson.loginId))
                {
                    Session.Clear();
                    return RedirectToAction("Login", "Person");
                }
                m_lSiteId = (int)objPerson.siteId;

                m_lRoleId = objPerson.roleId;
                if (m_lSiteId == 0)
                {
                    Session.Clear();
                    return RedirectToAction("Login", "Person");
                }
            }
            else
            {
                Session.Clear();
                return RedirectToAction("Login", "Person");
            }



            //gettting efolder list
            objeFolderDal = new eFolderDal();
            lsteFolders = objeFolderDal.GeteFolderList(m_lSiteId);
            lsteFolders = lsteFolders.OrderBy(x => x.SortSeq).ToList();
            //return View("model", objeFolderModel);

            return View(lsteFolders);

            //return View();

        }


        [HttpPost]
        public ActionResult FolderView(FormCollection collection)
        {
            objeFolderDal = new eFolderDal();
            var sortData = collection["SortSeq[]"];
            var folderIdData = collection["FolderId[]"];
            var folderNameData = collection["FolderName[]"];
            int siteid = 0;

            if (collection["hdnsiteid"] != "")
            {
                siteid = Convert.ToInt32(collection["hdnsiteid"]);
            }

            string strSortData = string.Empty;
            strSortData = string.Join(",", sortData);

            string strfolderIdData = string.Empty;
            strfolderIdData = string.Join(",", folderIdData);

            string strfolderNameData = string.Empty;
            strfolderNameData = string.Join(",", folderNameData);


            //Saveing data
            objeFolderDal.UpdateefolderSeqOrder(strfolderIdData, strSortData);

            //gettting efolder list

            lsteFolders = objeFolderDal.GeteFolderList(siteid);
            lsteFolders = lsteFolders.OrderBy(x => x.SortSeq).ToList();
            return View(lsteFolders);



        }

        [HttpGet]
        public ActionResult EditSiteFolderRecordView(int lintFolderId, string strFoldername)
        {
            objeFolderModel = new eFolderModel();
            objeFolderModel.FolderId = lintFolderId;
            objeFolderModel.FolderName = strFoldername;

            objeFolderDal = new eFolderDal();
            objeFolderModel.folderfileslist = objeFolderDal.GeteFolderListByFolderId(lintFolderId);

            return View(objeFolderModel);
        }


        //View Handouts
        public class Converter
        {
            public static byte[] GetByteArray(object o)
            {
                return (byte[])o;
            }
        }
        [HttpGet]
        public ActionResult NotesView(int lintFileId, int lintsiteid)
        {
            NotesModel objnotes = new NotesModel();
            //objnotes.Text = "sudhakar";
            objnotes.SiteId = lintsiteid;
            objnotes.FileId = lintFileId;
            objeFolderDal = new eFolderDal();
            objeFolderDal.GetFileNotesByFileID(ref objnotes);
            return View(objnotes);
        }

        [ValidateInput(false)]
        public ActionResult AddNoteDetails(int lintId, int lintFileId, int lintsiteid, string strDescription, string strtext)
        {
            NotesModel objnotes = new NotesModel();
            try
            {
                //objnotes.Text = "sudhakar";
                objnotes.Id = lintId;
                objnotes.SiteId = lintsiteid;
                objnotes.FileId = lintFileId;
                objnotes.Description = strDescription;
                objnotes.isDeleted = false;
                objnotes.Text = strtext;
                objnotes.LastUpdateDate = DateTime.Now;
                objeFolderDal = new eFolderDal();
                if (lintId > 0)
                    objeFolderDal.EditNote(ref objnotes);
                else
                    objeFolderDal.AddNotes(ref objnotes);

                objeFolderDal.GetFileNotesByFileID(ref objnotes);

                //return View(objnotes);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("NotesView", objnotes);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NotesView(NotesModel objnotes)
        {
            //return View(objnotes);

            objnotes.SiteId = 51;
            objnotes.FileId = 109;
            objeFolderDal = new eFolderDal();
            objeFolderDal.GetFileNotesByFileID(ref objnotes);
            return View(objnotes);
        }

        [HttpGet]
        public ActionResult ViewFrame(int lintFolderId)
        {
            objeFolderModel = new eFolderModel();
            int siteid = 0;

            if (Session["SiteId"] != null)
            {
                siteid = (int)Session["SiteId"];
            }

            if (siteid == 0)
            {
                Session.Clear();
                return RedirectToAction("Login", "Person");
            }
            objeFolderModel.FolderId = lintFolderId;
            objeFolderModel.SiteId = siteid;


            return View(objeFolderModel);
        }

        public ActionResult GetPDF(int id)
        {

            System.Collections.Hashtable ps = new System.Collections.Hashtable();
            objeFolderDal = new eFolderDal();
            Object oc = new Object();
            Object t = new Object();
            objeFolderModel = new eFolderModel();
            ps.Add("@id", id);
            ps.Add("@RoleId", 1);
            ps.Add("@ViewId", 2);
            ps.Add("@languageId", 1);

            t = objeFolderDal.GetFileTypeBasedOnRole("GetFileTypeBasedOnRole", ps);
            oc = objeFolderDal.GetFileTypeBasedOnRole("GetFileBasedOnRole", ps);

            return new BinaryContentResult(Converter.GetByteArray(oc), t.ToString());


        }

        public ActionResult ReturnPartial(int lintBookId, string strBookName)
        {

            PDFViewModel objPdfView = new PDFViewModel();
            objPdfView.Id = lintBookId;
            objPdfView.Name = strBookName;

            return PartialView("_PDFPartial", objPdfView);
            //return PartialView("_TabPartial");
        }

        public void PrintFile(int lintBookId, string strBookName)
        {

            try
            {
                PDFViewModel objPdfView = new PDFViewModel();
                objPdfView.Id = lintBookId;
                objPdfView.Name = strBookName;
                int languageId = 1;

                Response.Clear();
                Response.ContentType = Convert.ToString("application/pdf");
                //var b = Helper.Converter.GetByteArray(GetDocument(lintBookId, languageId).PDFDocument.GetData());
                
                Response.BinaryWrite(Helper.Converter.GetByteArray(GetDocument(lintBookId, languageId).PDFDocument.GetData()));
                Response.Flush();
                Response.End();


                //return PartialView("_PDFPartial", objPdfView);
                //return PartialView("_TabPartial");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            

        }




        public ActionResult PrintView(int lintBookId, string strBookName)
        {

            PDFViewModel objPdfView = new PDFViewModel();
            objPdfView.Id = lintBookId;
            objPdfView.Name = strBookName;

            //return PartialView("_PDFPartial", objPdfView);
            //return PartialView("_TabPartial");
            return View(objPdfView);
        }

        //public ActionResult PrintFile1(int lintBookId, string strBookName)
        //{

        //    PDFViewModel objPdfView = new PDFViewModel();
        //    objPdfView.Id = lintBookId;
        //    objPdfView.Name = strBookName;
        //    int languageId = 1;

        //    Response.Clear();
        //    Response.ContentType = Convert.ToString("application/pdf");
        //    var b = Helper.Converter.GetByteArray(GetDocument(lintBookId, languageId).PDFDocument.GetData());
        //    Response.BinaryWrite(Helper.Converter.GetByteArray(GetDocument(lintBookId, languageId).PDFDocument.GetData()));
        //    Response.Flush();
        //    Response.End();


        //    //return PartialView("_PDFPartial", objPdfView);
        //    //return PartialView("_TabPartial");

        //}

        public ActionResult ReturnNotesPartial(int lintBookId)
        {

            NotesModel objPdfView = new NotesModel();
            objeFolderDal = new eFolderDal();
            objPdfView.FileId = lintBookId;

            //if (Session["SiteId"]!=null)
            objPdfView.SiteId = (int)Session["SiteId"];
            //else


            objeFolderDal.GetFileNotesByFileID(ref objPdfView);
            return PartialView("_notesPartial", objPdfView);
        }

        public ActionResult ReturnCoverSheetPartial()
        {

            List<FileCoversheet> lstCoverSheets = new List<FileCoversheet>();
            objeFolderDal = new eFolderDal();
            int siteId = (int)Session["SiteId"];
            lstCoverSheets = objeFolderDal.GetFileCoversheetsBySiteId(siteId);
            return PartialView("_CoverSheetPartial", lstCoverSheets);
        }

        public ActionResult ReturnAddCoverSheetPartial(int fileId)
        {

            FileCoversheet objCoverSheets = new FileCoversheet();
            objeFolderDal = new eFolderDal();
            int siteId = (int)Session["SiteId"];

            return PartialView("_AddCoverSheetPartial");
        }

        //For Edit Section (Click on Section)

        public ActionResult ReturnSectionPartial(int lintSectionId, string strSectionName, int BookId, int SiteId)
        {

            SectionModel objSection = new SectionModel();
            objSection.SectionId = lintSectionId;
            objSection.SectionTitle = strSectionName;
            objSection.BookId = BookId;
            objSection.SiteId = SiteId;

            return PartialView("_SectionTabPartial", objSection);

        }



        public ActionResult PrintCount(int FolderId)
        {
            int printcount = 0;
            objeFolderDal = new eFolderDal();

            //   System.Collections.Hashtable ps = new System.Collections.Hashtable();
            //   objeFolderDal = new eFolderDal();
            //   Object oc = new Object();
            //   Object t = new Object();
            //   objeFolderModel = new eFolderModel();
            //   ps.Add("@id", FolderId);
            //   ps.Add("@RoleId", 1);
            //   ps.Add("@ViewId", 2);
            //   ps.Add("@languageId", 1);

            //   t = objeFolderDal.GetFileTypeBasedOnRole("GetFileTypeBasedOnRole", ps);
            //   oc = objeFolderDal.GetFileTypeBasedOnRole("GetFileBasedOnRole", ps);

            //var pdf=   new BinaryContentResult(Converter.GetByteArray(oc), t.ToString());





            //printcount=objeFolderDal.GetPrintCountHandoutViewsOrPrintsMonthwise()
            return Json(printcount, JsonRequestBehavior.AllowGet);
        }

        private clsPDF GetDocument(Int32 id, Int32 languageId)
        {
            Int32 siteId = default(Int32);
            siteId = Convert.ToInt32(Session["SiteId"]);

            clsPDF oPDFd = default(clsPDF);
            oPDFd = new clsPDF();

           oPDFd.AppendPage("~/Blank_pdf.pdf");

            System.Collections.Hashtable ps = new System.Collections.Hashtable();
            ps.Add("@id", id);
            ps.Add("@languageId", languageId);
            ps.Add("@UserId", Session["UserId"].ToString());

            object oc = null;
            oc = DB.ProcedureScalar("GetFile", ps);


            if ((oc != null))
            {
                byte[] b = null;
                b = Helper.Converter.GetByteArray(oc);
                //WebSupergoo.ABCpdf10.Doc d = default(WebSupergoo.ABCpdf10.Doc);
                //d = new WebSupergoo.ABCpdf10.Doc();
                WebSupergoo.ABCpdf10.Doc d = default(WebSupergoo.ABCpdf10.Doc);
                d = new WebSupergoo.ABCpdf10.Doc();
                d.Read(b);
                Int32 docPage = default(Int32);
                // d.AddImageUrl("http://www.google.com/", true, 0, true);
                oPDFd.PDFDocument.Append(d);
                // oPDF.PDFDocument.AddPage()






                clsPDF oPDF = default(clsPDF);
                oPDF = new clsPDF();

                string notes = null;
                string Description = null;
                System.Collections.Hashtable ps2 = new System.Collections.Hashtable();

                ps2.Add("@FileId", id);
                ps2.Add("@SiteId", Session["siteid"]);
                System.Data.DataTable dt = null;
                dt = DB.ProcedureTable("FileNotes_GetAll", ps2);
                Int32 i = default(Int32);

                if (dt.Rows.Count > 0)
                {
                    Description = string.Format("{0}", dt.Rows[i]["Description"]);
                    Int32 max = default(Int32);
                    max = dt.Rows.Count - 1;
                    for (i = 0; i <= max; i++)
                    {
                        notes = notes + string.Format("{0}<br/><br/>", dt.Rows[i]["Text"]);
                    }

                    //notes = HttpContext.Current.Server.UrlEncode(notes);
                    notes = System.Web.HttpUtility.UrlEncode(notes);
                    Session["ArvHandoutNotes"] = notes;
                    string sWebServer = null;
                    sWebServer = System.Configuration.ConfigurationSettings.AppSettings["WebServer"].ToString();


                    string userName = null;
                    try
                    {
                        userName = Session["UserId"].ToString();
                    }
                    catch (Exception ex)
                    {
                        userName = "";
                    }

                    //ClearCache()


                    string url = null;
                    TimeSpan? variable = DateTime.Today - DateTime.Now;
                    //url = sWebServer + "Files/FileNotesTemplate.aspx?UserName=" + userName + "&id=" + id + "&siteid=" + siteId + "&dummy=" + variable + "&Description=" + Description;

                    url = sWebServer + "FolderView/FileCoversheetTemplate";
                    url = "http://www.google.com/";

                    //url = "http://scripts.p-h.com/Files/FileNotesTemplate.aspx?UserName=407&id=5468&siteid=51&dummy=37425&Description=cancerLung"
                    //url = "http://localhost/phwap/Files/FileNotesTemplate.aspx?UserName=407&id=5468&siteid=51&dummy=37425&Description=cancerLung"
                    //url = "http://www.websupergoo.com/"

                    oPDF.PDFDocument.AddImageUrl(url, true, 0, true);


                    oPDFd.PDFDocument.Append(oPDF.PDFDocument);

                    // AddLogos(oPDFd);

                    //oPDFd.Compress()
                    //Helper.DB.ProcedureScalar("Files_PrintStat", ps)
                }
                else
                {
                    AddLogos(ref oPDFd, true);
                }
                oPDFd.Compress();

               

                DB.ProcedureScalar("Files_PrintStat", ps);
            }

            return oPDFd;

        }



        private void AddLogos(ref clsPDF oDoc, bool bNoNotes = false)
        {
            int iLoop = 1;
            XImage oIMG = new XImage();
            string sFullPath = "";
            double dblHeight = 0;
            double dblWidth = 0;
            int iDefW = 159;
            //225
            int iDefH = 36;
            //51
            double dblDiff = 0;
            int iLeft = 20;
            int iBottom = 0;
            try
            {
                sFullPath = RetrieveImage();
            }
            catch (Exception ex)
            {
                sFullPath = "";
            }
            if (!string.IsNullOrEmpty(sFullPath))
            {
                try
                {
                    int iCnt = oDoc.PageCount();
                    oIMG.SetFile(sFullPath);
                    oDoc.InsetRect(iLeft, iBottom);
                    ////Begin - Adjust Image by Percentage
                    //oDoc.RectHeight = oIMG.Height * 0.645
                    //oDoc.RectWidth = oIMG.Width * 0.645
                    ////End - Adjust Image by Percentage
                    ////Begin - Adjust Image to Specific Dimensions - Length as Base
                    //dblHeight = oIMG.Height
                    //dblWidth = oIMG.Width
                    //dblDiff = dblWidth / iDefW
                    //dblWidth = iDefW
                    //If dblDiff > 0 Then
                    //    dblHeight = dblHeight / dblDiff
                    //Else
                    //    dblHeight = iDefH
                    //End If
                    //oDoc.RectHeight = dblHeight
                    //oDoc.RectWidth = dblWidth
                    ////End - Adjust Image to Specific Dimensions - Length as Base
                    ////Begin - Use Exact Image Dimiensions
                    //oDoc.RectHeight = oIMG.Height
                    //oDoc.RectWidth = oIMG.Width
                    ////End - Use Exact Image Dimiensions
                    ////Begin - Adjust Image to Specific Dimensions - Height as Base
                    dblHeight = oIMG.Height;
                    dblWidth = oIMG.Width;
                    dblDiff = dblHeight / iDefH;
                    dblHeight = iDefH;
                    if (dblDiff > 0)
                    {
                        dblWidth = dblWidth / dblDiff;
                    }
                    else
                    {
                        dblWidth = iDefW;
                    }
                    oDoc.RectHeight = dblHeight;
                    oDoc.RectWidth = dblWidth;
                    ////End - Adjust Image to Specific Dimensions - Height as Base
                    if (bNoNotes)
                    {
                        //Skip the Initial Blank Page
                        for (iLoop = 2; iLoop <= iCnt; iLoop++)
                        {
                            oDoc.PageNumber = iLoop;
                            oDoc.AddImageObject(ref oIMG, true);
                        }
                    }
                    else
                    {
                        //Skip the Initial Blank Page and the Final "Notes" Page
                        for (iLoop = 2; iLoop <= (iCnt - 1); iLoop++)
                        {
                            oDoc.PageNumber = iLoop;
                            oDoc.AddImageObject(ref oIMG, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }



        private string RetrieveImage()
        {
            string functionReturnValue = null;
            //clsSQLReader oSQL = new clsSQLReader();
            //SqlDataReader oRead = default(SqlDataReader);
            //long lRet = 0;
            //string sTemp = null;
            //functionReturnValue = "";
            //try
            //{
            //    oSQL.ConnectionStringKey = "ConnectionString";
            //    oSQL.Add_Parm("@SiteID", Convert.ToInt64(Session["SiteID"]));
            //    oRead = oSQL.ExecuteReader("sp_Ret_SiteInfo");
            //    if ((oRead != null))
            //    {
            //        if (oRead.HasRows)
            //        {
            //            oRead.Read();
            //            if (!Information.IsDBNull(oRead("BrandFooterFilePath")))
            //            {
            //                sTemp = Convert.ToString(oRead("BrandFooterFilePath"));
            //                sTemp = Server.MapPath("~/Assets/Staging/SiteBrands/" + sTemp);
            //                functionReturnValue = sTemp;
            //            }
            //        }
            //        oRead.Close();
            //        oRead = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //finally
            //{
            //    if (Information.TypeName(oRead) == "SqlDataReader")
            //    {
            //        if (!oRead.IsClosed)
            //        {
            //            oRead.Close();
            //        }
            //        oRead = null;
            //    }
            //    oSQL.Dispose();
            //    oSQL = null;
            //}
            return functionReturnValue;
        }


        //Add cover sheet and print
        [AllowAnonymous]
        public ActionResult FileCoversheetTemplate()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult AddCoversheetPrint(int lintId, int lintFileId, int lintsiteid, string strDescription, string strtext)
        {
            NotesModel objnotes = new NotesModel();
            try
            {

                Hashtable ps = new Hashtable();
                Int32 siteId = default(Int32);
                siteId = Convert.ToInt32(Session["SiteId"]);
                ps.Add("@siteId", siteId);
                ps.Add("@text", strtext);
                ps.Add("@name", strDescription);
                DB.ProcedureScalar("FileCoversheets_Add", ps);

                string text = null;
                text = Server.UrlEncode(strtext);

                string sWebServer = null;
                //sWebServer = System.Configuration.ConfigurationSettings.AppSettings["WebServer"].ToString();
                sWebServer = "http://localhost:51729/";

                string userName = null;
                try
                {
                    userName = Session["UserId"].ToString();
                }
                catch (Exception ex)
                {
                    userName = "";
                }



                clsPDF oPDF = default(clsPDF);
                oPDF = new clsPDF();

                //if (((Request("action") != null)))
                //{
                //    if (Request("action").ToString == "all")
                //    {
                oPDF.AppendPage("Blank_pdf.pdf");

                Int32 Id = default(Int32);
                Id = lintFileId;// Convert.ToInt32(Request("id"));

                Int32 languageId = default(Int32);
                object olid = null;
                olid = 1;// Request.Params("languageId");

                //if ((olid == null))
                //{
                //    languageId = Convert.ToInt32(HttpContext.Current.Session["SelectedLanguageId"]);
                //}
                //else
                //{
                //    languageId = Convert.ToInt32(olid);
                //}

                languageId = 1;

                oPDF.PDFDocument.Append(this.GetDocument(Id, languageId).PDFDocument);
                oPDF.PageNumber = 2;
                //    }
                //    else
                //    {
                //        oPDF.AppendPage("Blank_pdf.pdf");
                //    }
                //}
                //else
                //{
                //    oPDF.AppendPage("Blank_pdf.pdf");
                //}

                string sName = "abc";

                // oPDF.AddURL(sWebServer + "/Files/FileCoversheetTemplate.aspx?UserName=" + userName + "&PatientName=" + sName + "&Note=" + text);
                oPDF.AddURL(sWebServer + "/FolderViewController/FileCoversheetTemplate");
                //Helper.Log.Save(sWebServer + "/Files/FileCoversheetTemplate.aspx?UserName=" + userName + "&PatientName=" + sName + "&Note=" + text);
                oPDF.Compress();

                Response.Clear();
                Response.ContentType = "application/pdf";
                //Response.BinaryWrite(Helper.Converter.GetByteArray(oPDF.PDFDocument.GetData()));

                return new BinaryContentResult(Converter.GetByteArray(oPDF.PDFDocument.GetData()), "application/pdf");

                //Response.Flush();
                //Response.End();


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





    }






    #region "BinaryContentResult"
    public class BinaryContentResult : ActionResult
    {
        private string ContentType;
        private byte[] ContentBytes;

        public BinaryContentResult(byte[] contentBytes, string contentType)
        {
            // TODO: Complete member initialization
            this.ContentBytes = contentBytes;
            this.ContentType = contentType;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.Clear();
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.ContentType = this.ContentType;

            var stream = new MemoryStream(this.ContentBytes);
            stream.WriteTo(response.OutputStream);
            stream.Dispose();
        }
    }
    #endregion



}