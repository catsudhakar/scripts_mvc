﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.Models;
using TBScripts.DAL;

namespace TBScripts.Controllers
{
    public class BaseController : Controller
    {
        //
        // GET: /Base/
        
        PersonModel objPerson = null;
       public PersonModel UDTO = null; //User Data Trammssion object
        eFolderModel objeFolderModel = null;
        List<eFolderModel> lsteFolders = null;
        eFolderDal objeFolderDal;

        const long NURSE_ROLE = 1;
        const long SITE_ADMIN_ROLE = 2;
        const long PH_EMPLOYEE_ROLE = 3;
        const long SYSTEM_ADMIN_ROLE = 4;
        const long AUTHOR_ROLE = 5;
        const long REVIEWER_ROLE = 6;
        const long VISITOR_ROLE = 9;
        const long NURSE_ROLE_New = 10;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var currentURL = Request.RawUrl;
            //var currentURL1 =Request.Url.ToString();

            if (Session["Person"] != null)
            {
                UDTO = (PersonModel)Session["Person"];
                if (UDTO.siteId > 0)
                {
                    var roles = "";
                    var controller = "SiteAdmin";
                    var descriptor = filterContext.ActionDescriptor;
                    var actionName = descriptor.ActionName;
                    var controllerName = descriptor.ControllerDescriptor.ControllerName;

                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    TempData["LoginErrorMessage"] = "You are not authorized to view this page..please contact your administrator..";
                    filterContext.Result = Redirect(Url.Content("~/Person/Login"));
                  
                }
            }
            else
            {
                //TODO: Find a better way of getting the session?
               
                TempData["LoginErrorMessage"] = "Your session has expired..please login here..";
                filterContext.Result = Redirect(Url.Content("~/Person/Login"));
            }
        }


        public BaseController()
        {

            MyeFolderModel objMyefoldermodel = new MyeFolderModel();
            objPerson = new PersonModel();
            objeFolderModel = new eFolderModel();
            lsteFolders = new List<eFolderModel>();


            int m_lSiteId = 0;
            int m_lRoleId = 0;
            bool m_bShowEditLinks = false;
            string m_sRootNodeTile = string.Empty;
            if (System.Web.HttpContext.Current.Session["Person"] != null)
            {
                objPerson = (PersonModel)System.Web.HttpContext.Current.Session["Person"];
              
                m_lSiteId = (int)objPerson.siteId;

                m_lRoleId = objPerson.roleId;
                
            }

            m_sRootNodeTile = "eFolders for " + objPerson.siteName;

            if (m_lRoleId == SYSTEM_ADMIN_ROLE)
                m_bShowEditLinks = true;
            else if (m_lRoleId == AUTHOR_ROLE)
                m_bShowEditLinks = true;
            else
                m_bShowEditLinks = false;

            objMyefoldermodel.m_sRootNodeTile = m_sRootNodeTile;
            objMyefoldermodel.m_bShowEditLinks = m_bShowEditLinks;

            //gettting efolder list
            objeFolderDal = new eFolderDal();
            lsteFolders = objeFolderDal.GeteFolderList(m_lSiteId);
            objMyefoldermodel.lsteFolderModel = lsteFolders;
            ViewBag.LayoutModel = objMyefoldermodel;
        }


        //public ActionResult Index()
        //{
        //    return View();
        //}
    }
}