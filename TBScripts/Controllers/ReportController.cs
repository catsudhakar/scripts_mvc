﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.DAL;
using TBScripts.Models;

namespace TBScripts.Controllers
{
    public class ReportController : BaseController
    {
        //
        // GET: /Report/
        public ActionResult Index()
        {
            return View();
        }

        public List<PreNotifyListModel> GetNotificationReport(int BookList)
        {

            ReportListModel reports = new ReportListModel();
            PreNotifyListModel objTreeView = new PreNotifyListModel();
            List<PreNotifyListModel> PreNotifyList = new List<PreNotifyListModel>();
            TreeViewDal objTreeViewDal = new TreeViewDal();

            PreNotifyList = objTreeViewDal.GetPreNotifyReportByBookId(BookList);

            if (PreNotifyList.Count > 0)
            {
                foreach (var item in PreNotifyList)
                {
                    //get files per sections
                    item.FileStatus = objTreeViewDal.GetFileStausBySectionId(item.SectionId);
                }
            }

            //model.treeViewModel = lstTreeView;

            //TreeViewModel objTreeView1 = new TreeViewModel();
            //List<TreeViewModel> lstTreeView1 = new List<TreeViewModel>();
            //lstTreeView1 = objTreeViewDal.GetTreeView();


            //int lintPrvBookId = 0;
            //List<TreeViewModel> t = new List<TreeViewModel>();
            //foreach (var book in lstTreeView1)
            //{
            //    if (book.intBookId != lintPrvBookId)
            //    {
            //        t.Add(book);
            //    }
            //    lintPrvBookId = book.intBookId;
            //}

            //TempData["BooksItems"] = t;

            return PreNotifyList;// View(reports);

            //return Json("Sucess");
        }

        public ActionResult PreNotifications()
        {

            List<TBScripts.Models.BookModel> Books = new List<TBScripts.Models.BookModel>();
            ReportListModel reports = new ReportListModel();
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    Books = DBContext.Books.Select(x => new BookModel
                    {
                        strBookTitle = x.BookTitle,
                        intBookId = x.BookId
                    }).ToList();

                    reports.BookList = new SelectList(Books, "intBookId", "strBookTitle");



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(reports);

            //  return View();
        }
        [HttpPost]
        public ActionResult PreNotifications(int BookList)
        {

            List<TBScripts.Models.BookModel> Books = new List<TBScripts.Models.BookModel>();
            ReportListModel reports = new ReportListModel();
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    Books = DBContext.Books.Select(x => new BookModel
                    {
                        strBookTitle = x.BookTitle,
                        intBookId = x.BookId
                    }).ToList();

                    reports.BookList = new SelectList(Books, "intBookId", "strBookTitle");



                }
                reports.PreNotifyList = GetNotificationReport(BookList);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(reports);

            //  return View();
        }



        public ActionResult SiteUsage()
        {
            return View();
        }

        public ActionResult Content()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PreNotifications1(int? lintBookId)
        {
            Mymodel model = new Mymodel();
            TreeViewModel objTreeView = new TreeViewModel();
            List<TreeViewModel> lstTreeView = new List<TreeViewModel>();
            TreeViewDal objTreeViewDal = new TreeViewDal();

            lstTreeView = objTreeViewDal.GetTreeViewByBookId(lintBookId);
            model.treeViewModel = lstTreeView;

            TreeViewModel objTreeView1 = new TreeViewModel();
            List<TreeViewModel> lstTreeView1 = new List<TreeViewModel>();
            lstTreeView1 = objTreeViewDal.GetTreeView();


            int lintPrvBookId = 0;
            List<TreeViewModel> t = new List<TreeViewModel>();
            foreach (var book in lstTreeView1)
            {
                if (book.intBookId != lintPrvBookId)
                {
                    t.Add(book);
                }
                lintPrvBookId = book.intBookId;
            }

            TempData["BooksItems"] = t;

            return View(model);
        }

        public PartialViewResult ChaptersBySectionID(int SectionId)
        {
            List<SectionChapters> lstTreeView = new List<SectionChapters>();
            TreeViewDal objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetChaptersBySectionID(SectionId);
            return PartialView("_PartialChapters", lstTreeView);
        }

        public PartialViewResult HandoutsByChapterID(int ChapterId)
        {
            List<ChapterHandOuts> lstTreeView = new List<ChapterHandOuts>();
            TreeViewDal objTreeViewDal = new TreeViewDal();
            lstTreeView = objTreeViewDal.GetHandoutsByChapterID(ChapterId);
            return PartialView("_PartialHandouts", lstTreeView);
        }

        /* Total usage report */
        [HttpPost]
        public PartialViewResult GetTotalUsage(int SiteList, int Languages, string StartDate, string EndDate)
        {

            try
            {
                TotalUsageStatusReport t = new TotalUsageStatusReport();
                List<HandoutCounts> counts = new List<HandoutCounts>();
                TreeViewDal objTreeViewDal = new TreeViewDal();

                if (StartDate == null)
                    StartDate = "";
                if (EndDate == null)
                    EndDate = "";

                counts = objTreeViewDal.GetTotalUsageReport(SiteList, Languages, StartDate, EndDate);

                //HandoutCounts count = new HandoutCounts();
                //count.HandoutId = 1;
                //count.HandoutTitle = "fdfsdfsd";
                //count.ViewsCount = 10;
                //count.PrintCounts = 4;
                //counts.Add(count);
                t.HandoutCounts = counts;


                return PartialView("_TotalUsageReportPartial", t);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public ActionResult TotalUsage()
        {
            List<TBScripts.Models.SiteReport> Sites = new List<TBScripts.Models.SiteReport>();
            List<TBScripts.Models.LanguageModel> Languages = new List<TBScripts.Models.LanguageModel>();
            TotalUsageStatusReport reports = new TotalUsageStatusReport();
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    Sites = DBContext.Sites.Select(x => new SiteReport
                    {
                        SiteId = x.SiteId,
                        SiteTitle = x.SiteName
                    }).ToList();

                    reports.SiteList = new SelectList(Sites, "SiteId", "SiteTitle");

                    Languages = DBContext.Languages.Select(x => new LanguageModel
                    {
                        intlanguageId = x.LanguageId,
                        strlanguageName = x.LanguageName
                    }).ToList();

                    reports.Languages = new SelectList(Languages, "intlanguageId", "strlanguageName");

                }



            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(reports);
        }


    }
}