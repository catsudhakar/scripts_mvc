﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.DAL;
using TBScripts.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

using System.Text.RegularExpressions;

namespace TBScripts.Controllers
{
    public class SystemAdminController : BaseController
    {
        // GET: SystemAdmin
        // PHdataConnectionString1 DBContext = new PHdataConnectionString1();
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult SetupSystem()
        {
            return View();
        }

        public ActionResult AddCustomer(int custId)
        {
            CustomerModel model = new CustomerModel();
            if (custId > 0)
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    var dblocations = DBContext.Customers.Where(location => location.CustomerId == custId).SingleOrDefault();
                    if (dblocations != null)
                    {

                        model.CustomerId = dblocations.CustomerId;
                        model.CustomerName = dblocations.CustomerName;
                        model.Address1 = dblocations.Address1;
                        model.Address3 = dblocations.Address3;
                        model.Address2 = dblocations.Address2;
                        model.City = dblocations.City;
                        model.IsActive = (bool)dblocations.IsActive;
                        model.PostalCode = dblocations.PostalCode;
                        model.StateId = dblocations.StateId;
                        model.Notes = dblocations.Notes;

                    }
                }
            }

            model.StatesList = getListofStates();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddCustomer(CustomerModel model)
        {
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (model.CustomerId > 0)
                    {
                        var dblocations = DBContext.Customers.Where(location => location.CustomerId == model.CustomerId).SingleOrDefault();
                        if (dblocations != null)
                        {
                            //model.CustomerId = dblocations.CustomerId;
                            dblocations.CustomerName = model.CustomerName;
                            dblocations.Address1 = model.Address1;
                            dblocations.Address3 = model.Address3;
                            dblocations.Address2 = model.Address2;
                            dblocations.City = model.City;
                            dblocations.IsActive = model.IsActive;
                            dblocations.PostalCode = model.PostalCode;
                            dblocations.StateId = model.StateId;
                            dblocations.Notes = model.Notes;

                            DBContext.SaveChanges();

                        }
                    }
                    else
                    {
                        Customer AddCust = new Customer();
                        AddCust.CustomerName = model.CustomerName;
                        AddCust.Address2 = model.Address2;
                        AddCust.Address3 = model.Address3;
                        AddCust.City = model.City;
                        AddCust.StateId = model.StateId;
                        AddCust.PostalCode = model.PostalCode;
                        AddCust.Notes = model.Notes;
                        AddCust.Address1 = model.Address1;
                        AddCust.IsActive = model.IsActive;
                        DBContext.Customers.Add(AddCust);
                        DBContext.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ViewCustomer");
        }

        public ActionResult ViewCustomer()
        {

            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    var dblocations = DBContext.Customers.Where(location => location.IsActive == true).ToList();
                    foreach (var item in dblocations)
                    {
                        CustomerModel cust = new CustomerModel();
                        cust.CustomerId = item.CustomerId;
                        cust.CustomerName = item.CustomerName;
                        cust.Address1 = item.Address1;
                        cust.Address3 = item.Address3;
                        cust.Address2 = item.Address2;
                        cust.City = item.City;
                        cust.IsActive = (bool)item.IsActive;
                        cust.PostalCode = item.PostalCode;
                        cust.StateId = item.StateId;
                        cust.Notes = item.Notes;
                        lstCustomerModel.Add(cust);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstCustomerModel);
        }

        //Get Product line

        public ActionResult ViewProductLine()
        {

            ListProductLineTypeModel ProductLineTypeModel1 = new ListProductLineTypeModel();
            List<ProductLineModel> lstProductLineModel = new List<ProductLineModel>();
            List<ProductLineType> dbProductLineType = new List<ProductLineType>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbProductLine = from s in DBContext.ProductLines
                                        join sa in DBContext.ProductLineTypes
                                        on s.ProductLineTypeId equals sa.ProductLineTypeId
                                        select s;
                    foreach (var item in dbProductLine)
                    {
                        ProductLineModel prod = new ProductLineModel();
                        prod.ProductLineId = item.ProductLineId;
                        prod.ProductLineDesc = item.ProductLineDesc;
                        prod.ProductLineTypeId = (int)item.ProductLineTypeId;
                        prod.strProductLineTypeDesc = item.ProductLineType.ProductLineTypeDesc;
                        prod.CopyrightDesc = item.CopyrightDesc;

                        lstProductLineModel.Add(prod);
                    }

                    dbProductLineType = DBContext.ProductLineTypes.ToList();

                    ProductLineTypeModel1.lstProductLine = lstProductLineModel;
                    ProductLineTypeModel1.lstProductLineType = dbProductLineType;
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            //return View(lstProductLineModel);
            return View(ProductLineTypeModel1);
        }

        public List<StateModel> getListofStates()
        {
            List<StateModel> StatesList = new List<StateModel>();
            StateModel prod1 = new StateModel();
            prod1.StateId = "";
            prod1.StateName = "Please Select";
            StatesList.Add(prod1);
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbstates = from s in DBContext.StateProvinces select s;
                    foreach (var item in dbstates)
                    {
                        StateModel prod = new StateModel();
                        prod.StateId = item.StateId;
                        prod.StateName = item.StateName;
                        StatesList.Add(prod);
                    }
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return StatesList;
        }
        public List<Site> GetSites()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbsites = DBContext.Sites.Where(s => s.IsActive == true).OrderBy(s => s.SiteName).ToList();
                return dbsites;

            }
        }

        public List<Role> GetRoles()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbrole = DBContext.Roles.OrderBy(p => p.RoleName).ToList();
                return dbrole;

            }
        }

        public List<PhoneType> GetPhoneType()
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbPhoneType = DBContext.PhoneTypes.OrderBy(p => p.PhoneTypeDesc).ToList();
                return dbPhoneType;

            }
        }

        public PersonModel GetPersonByPersonId(int PersonId)
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {
             
               
                var dbPerson = DBContext.People.Where(x => x.PersonId == PersonId).Select(x => new PersonModel
                {

                    firstName = x.FirstName,
                    lastName = x.LastName,
                    siteName = x.Site.SiteName,
                    emailAddress = x.EmailAddress,
                    loginId = x.LoginId,
                    isActive = x.IsActive,
                    roleName = x.Role.RoleName,
                    notes = x.Notes,
                    phone1TypeId = x.Phone1TypeId,
                    phone2TypeId = x.Phone2TypeId,
                    phone3TypeId = x.Phone3TypeId,
                    phone4TypeId = x.Phone4TypeId,
                    password = x.Password,
                    phoneNumber1 = x.PhoneNumber1,
                    phoneNumber2 = x.PhoneNumber2,
                    phoneNumber3 = x.PhoneNumber3,
                    phoneNumber4 = x.PhoneNumber4,
                    personId=x.PersonId,
                    siteId=x.SiteId,
                    roleId=x.RoleId

                }).SingleOrDefault();


                return dbPerson;

            }
        }

        public ActionResult AddPersonRecord(int PersonId)
        {
            PersonModel model = new PersonModel();
            List<Site> lstSite = new List<Site>();
            List<PhoneType> lstPhoneType = new List<PhoneType>();
            List<Role> lstRole = new List<Role>();

            try
            {
              
              
                //using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                //{

                //    var dbsites = DBContext.Sites.Where(s => s.IsActive == true).OrderBy(s => s.SiteName).ToList();
                //    if (dbsites != null)
                //    {
                //        foreach (var item in dbsites)
                //        {
                //            Site objSite = new Site();
                //            objSite.SiteId = item.SiteId;
                //            objSite.SiteName = item.SiteName;
                //            lstSite.Add(objSite);
                //        }
                //    }
                //}

                //using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                //{

                //    var dbPhoneType = DBContext.PhoneTypes.OrderBy(p => p.PhoneTypeDesc).ToList();
                //    if (dbPhoneType != null)
                //    {
                //        foreach (var item in dbPhoneType)
                //        {
                //            PhoneType objPhoneType = new PhoneType();
                //            objPhoneType.PhoneTypeId = item.PhoneTypeId;
                //            objPhoneType.PhoneTypeDesc = item.PhoneTypeDesc;
                //            lstPhoneType.Add(objPhoneType);
                //        }
                //    }
                //}

                //using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                //{

                //    var dbrole = DBContext.Roles.OrderBy(p => p.RoleName).ToList();
                //    if (dbrole != null)
                //    {
                //        foreach (var item in dbrole)
                //        {
                //            Role objRole = new Role();
                //            objRole.RoleId = item.RoleId;
                //            objRole.RoleName = item.RoleName;
                //            lstRole.Add(objRole);
                //        }
                //    }
                //}

                //lstSite.Insert(0, new Site { SiteId = 0, SiteName = "Please select" });
                // lstPhoneType.Insert(0, new PhoneType { PhoneTypeId = 0, PhoneTypeDesc = "Please select" });
                //lstRole.Insert(0, new Role { RoleId = 0, RoleName = "Please select" });

                lstSite = GetSites();
                lstRole = GetRoles();
                lstPhoneType = GetPhoneType();

                if (PersonId > 0)
                {
                    model = GetPersonByPersonId(PersonId);
                }
                model.lstSite = lstSite;
                model.lstPhoneType = lstPhoneType;
                model.lstRole = lstRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            

            return View(model);
        }


        public int VerifyLoginName(string strLoginName)
        {
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbrole = DBContext.People.Where(p => p.LoginId == strLoginName).Count();
                return dbrole;

            }
        }

        [HttpPost]
        public ActionResult AddPersonRecord(PersonModel model)
        {
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (model.personId > 0)
                    {
                        var dbPerson = DBContext.People.Where(location => location.PersonId == model.personId).SingleOrDefault();
                        if (dbPerson != null)
                        {

                            dbPerson.FirstName = model.firstName;
                            dbPerson.LastName = model.lastName;
                            dbPerson.SiteId = model.siteId;
                            dbPerson.LoginId = model.loginId;
                            dbPerson.Password = model.password;
                            dbPerson.IsActive = model.isActive;
                            dbPerson.RoleId = model.roleId;
                            dbPerson.EmailAddress = model.emailAddress;
                            dbPerson.Notes = model.notes;
                            dbPerson.Phone1TypeId = model.phone1TypeId;
                            dbPerson.Phone2TypeId = model.phone2TypeId;
                            dbPerson.Phone3TypeId = model.phone3TypeId;
                            dbPerson.Phone4TypeId = model.phone4TypeId;

                            dbPerson.PhoneNumber1 = model.phoneNumber1;
                            dbPerson.PhoneNumber2 = model.phoneNumber2;
                            dbPerson.PhoneNumber3 = model.phoneNumber3;
                            dbPerson.PhoneNumber4 = model.phoneNumber4;

                            DBContext.Entry(dbPerson).State = System.Data.Entity.EntityState.Modified;
                            DBContext.SaveChanges();

                        }
                    }
                    else
                    {
                        TBScripts.DAL.Person AddCust = new TBScripts.DAL.Person();
                        AddCust.PersonId = model.personId;
                        AddCust.FirstName = model.firstName;
                        AddCust.LastName = model.lastName;
                        AddCust.SiteId = model.siteId;
                        AddCust.LoginId = model.loginId;
                        AddCust.Password = model.password;
                        AddCust.Notes = model.notes;
                        AddCust.RoleId = model.roleId;
                        AddCust.EmailAddress = model.emailAddress;
                        AddCust.IsActive = model.isActive;
                        AddCust.Phone1TypeId = model.phone1TypeId;
                        AddCust.Phone2TypeId = model.phone2TypeId;
                        AddCust.Phone3TypeId = model.phone3TypeId;
                        AddCust.Phone4TypeId = model.phone4TypeId;

                        AddCust.PhoneNumber1 = model.phoneNumber1;
                        AddCust.PhoneNumber2 = model.phoneNumber2;
                        AddCust.PhoneNumber3 = model.phoneNumber3;
                        AddCust.PhoneNumber4 = model.phoneNumber4;


                        DBContext.People.Add(AddCust);
                        DBContext.SaveChanges();
                    }

                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return RedirectToAction("ViewSystemUser", "Person");
        }

        #region "sites"
        public ActionResult ViewSites()
        {

            List<SiteModel> lstCustomerModel = new List<SiteModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.Sites.Where(location => location.IsActive == true).Take(100).ToList();
                    var dblocations = from s in DBContext.Sites
                                      join sa in DBContext.Customers
                                      on s.CustomerId equals sa.CustomerId
                                      where sa.IsActive == true
                                      select s;
                    foreach (var item in dblocations)
                    {
                        SiteModel cust = new SiteModel();
                        cust.SiteId = item.SiteId;
                        cust.SiteName = item.SiteName;
                        cust.CustomerId = item.CustomerId;
                        cust.CustomerName = item.Customer.CustomerName;
                        cust.SiteNumber = item.SiteNumber;
                        cust.BrandFooterFilePath = item.BrandFooterFilePath;
                        cust.BrandTypeId = item.BrandTypeId;
                        cust.ApplicationHeaderFilePath = item.ApplicationHeaderFilePath;
                        cust.SuppressOurBrand = item.SuppressOurBrand;
                        cust.Address1 = item.Address1;
                        cust.Address3 = item.Address3;
                        cust.Address2 = item.Address2;
                        cust.City = item.City;
                        cust.StateId = item.StateId;
                        cust.StateName = item.StateId != null ? item.StateProvince.StateName : "";
                        cust.PostalCode = item.PostalCode;
                        cust.PhoneNumber = item.PhoneNumber;
                        cust.FaxNumber = item.FaxNumber;
                        cust.Notes = item.Notes;
                        cust.IsActive = (bool)item.IsActive;
                        lstCustomerModel.Add(cust);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstCustomerModel);
        }
        public ActionResult AddSite(int SiteId)
        {
            SiteModel model = new SiteModel();
            List<CustomerModel> lstCustomerModel = new List<CustomerModel>();
            List<BrandTypeModel> lstBrandTypeModel = new List<BrandTypeModel>();
            List<StateModel> lstStateModel = new List<StateModel>();

            if (SiteId > 0)
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    var dbsite = DBContext.Sites.Where(s => s.SiteId == SiteId).SingleOrDefault();
                    if (dbsite != null)
                    {
                        model.SiteId = dbsite.SiteId;
                        model.SiteName = dbsite.SiteName;
                        model.SiteNumber = dbsite.SiteNumber;
                        model.CustomerId = dbsite.CustomerId;
                        model.FaxNumber = dbsite.FaxNumber;
                        model.PhoneNumber = dbsite.PhoneNumber;
                        model.Address1 = dbsite.Address1;
                        model.Address2 = dbsite.Address2;
                        model.Address3 = dbsite.Address3;
                        model.StateId = dbsite.StateId;
                        model.City = dbsite.City;
                        model.PostalCode = dbsite.PostalCode;
                        model.IsActive = (bool)dbsite.IsActive;
                        model.Notes = dbsite.Notes;
                        model.BrandTypeId = dbsite.BrandTypeId;
                        model.SuppressOurBrand = dbsite.SuppressOurBrand;
                        model.BrandFooterFilePath = dbsite.BrandFooterFilePath;
                        model.BrandHeaderFilePath = dbsite.BrandHeaderFilePath;
                        model.ApplicationHeaderFilePath = dbsite.ApplicationHeaderFilePath;
                    }


                }
            }

            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbCustomers = DBContext.Customers.Where(s => s.IsActive == true).OrderBy(s => s.CustomerName).ToList();
                if (dbCustomers != null)
                {
                    foreach (var item in dbCustomers)
                    {
                        CustomerModel objCustomerModel = new CustomerModel();
                        objCustomerModel.CustomerId = item.CustomerId;
                        objCustomerModel.CustomerName = item.CustomerName;
                        lstCustomerModel.Add(objCustomerModel);
                    }
                }
            }
            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {

                var dbCustomers = DBContext.BrandTypes.ToList();
                if (dbCustomers != null)
                {
                    foreach (var item in dbCustomers)
                    {
                        BrandTypeModel objBrandTypeModel = new BrandTypeModel();
                        objBrandTypeModel.BrandTypeId = item.BrandTypeId;
                        objBrandTypeModel.BrandTypeDesc = item.BrandTypeDesc;
                        lstBrandTypeModel.Add(objBrandTypeModel);
                    }
                }
            }

            using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
            {
                var dbstates = from s in DBContext.StateProvinces select s;
                foreach (var item in dbstates)
                {
                    StateModel prod = new StateModel();
                    prod.StateId = item.StateId;
                    prod.StateName = item.StateName;
                    lstStateModel.Add(prod);
                }
            }
            model.lstCustomer = lstCustomerModel;
            model.lstBrandType = lstBrandTypeModel;
            model.lstStateProvince = lstStateModel;


            return View(model);
        }
        [HttpPost]
        public ActionResult AddSite(SiteModel model, HttpPostedFileBase FileBrandFooterFilePath, HttpPostedFileBase FileBrandHeaderFilePath, HttpPostedFileBase FileApplicationHeaderFilePath)
        {


            //var r = new Random();
            string savefile = string.Empty;
            string FooterFileName = string.Empty;
            string HeaderFileName = string.Empty;
            string ApplicationFileName = string.Empty;
            if (FileBrandFooterFilePath != null)
            {
                if (FileBrandFooterFilePath.FileName != null && FileBrandFooterFilePath.ContentLength > 0)
                {
                    if (FileBrandFooterFilePath.ContentType.ToLower() == "image/gif" || FileBrandFooterFilePath.ContentType.ToLower() == "image/jpg" || FileBrandFooterFilePath.ContentType.ToLower() == "image/jpeg" || FileBrandFooterFilePath.ContentType.ToLower() == "image/png" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/svg")
                    {
                        model.BrandFooterFilePath = FileBrandFooterFilePath.FileName;
                        FooterFileName = FileBrandFooterFilePath.FileName;
                        savefile = "~/SiteBrands/" + FileBrandFooterFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileBrandFooterFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }
            if (FileBrandHeaderFilePath != null)
            {
                if (FileBrandHeaderFilePath.FileName != null && FileBrandHeaderFilePath.ContentLength > 0)
                {
                    if (FileBrandHeaderFilePath.ContentType.ToLower() == "image/gif" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/jpg" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/jpeg" || FileBrandHeaderFilePath.ContentType.ToLower() == "image/png" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/svg")
                    {
                        model.BrandHeaderFilePath = FileBrandHeaderFilePath.FileName;
                        HeaderFileName = FileBrandHeaderFilePath.FileName;
                        savefile = "~/SiteBrands/" + FileBrandHeaderFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileBrandHeaderFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }
            if (FileApplicationHeaderFilePath != null)
            {

                if (FileApplicationHeaderFilePath.FileName != null && FileApplicationHeaderFilePath.ContentLength > 0)
                {
                    if (FileApplicationHeaderFilePath.ContentType.ToLower() == "image/gif" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/jpg" || 
                        FileApplicationHeaderFilePath.ContentType.ToLower() == "image/jpeg" || FileApplicationHeaderFilePath.ContentType.ToLower() == "image/png" ||
                        FileApplicationHeaderFilePath.ContentType.ToLower() == "image/svg")
                    {
                        savefile = "~/SiteBrands/" + FileApplicationHeaderFilePath.FileName;
                        model.ApplicationHeaderFilePath = FileApplicationHeaderFilePath.FileName;
                        ApplicationFileName = FileApplicationHeaderFilePath.FileName;
                        //savefile = Server.MapPath("~/SiteBrands/" + FileBrandFooterFilePath.FileName);
                        if (!System.IO.File.Exists(Server.MapPath(savefile)))
                        {
                            FileApplicationHeaderFilePath.SaveAs(Server.MapPath(savefile));
                        }


                    }
                }
            }
            try
            {
                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (model.SiteId == 0)
                    {
                        //Adding 

                        TBScripts.DAL.Site AddCust = new TBScripts.DAL.Site();

                        AddCust.SiteName = model.SiteName;
                        AddCust.SiteNumber = model.SiteNumber;
                        AddCust.CustomerId = model.CustomerId;
                        AddCust.BrandFooterFilePath = FooterFileName;
                        AddCust.BrandHeaderFilePath = HeaderFileName;
                        AddCust.BrandTypeId = model.BrandTypeId;
                        AddCust.ApplicationHeaderFilePath = ApplicationFileName;
                        AddCust.SuppressOurBrand = model.SuppressOurBrand;
                        AddCust.Address1 = model.Address1;
                        AddCust.Address2 = model.Address2;
                        AddCust.Address3 = model.Address3;
                        AddCust.City = model.City;
                        AddCust.StateId = model.StateId;

                        AddCust.PostalCode = model.PostalCode;
                        AddCust.PhoneNumber = model.PhoneNumber;
                        AddCust.FaxNumber = model.FaxNumber;
                        AddCust.IsActive = model.IsActive;
                        AddCust.Notes = model.Notes;


                        DBContext.Sites.Add(AddCust);
                        DBContext.SaveChanges();

                    }
                    else
                    {
                        //Update
                        TBScripts.DAL.Site AddCust = new TBScripts.DAL.Site();
                        var dblocations = DBContext.Sites.Where(s => s.SiteId == model.SiteId).FirstOrDefault();
                        dblocations.SiteName = model.SiteName;
                        dblocations.SiteNumber = model.SiteNumber;

                        dblocations.CustomerId = model.CustomerId;
                        dblocations.BrandTypeId = model.BrandTypeId;

                        dblocations.SuppressOurBrand = model.SuppressOurBrand;
                        dblocations.Address1 = model.Address1;
                        dblocations.Address2 = model.Address2;
                        dblocations.Address3 = model.Address3;
                        dblocations.City = model.City;
                        dblocations.StateId = model.StateId;

                        dblocations.PostalCode = model.PostalCode;
                        dblocations.PhoneNumber = model.PhoneNumber;
                        dblocations.FaxNumber = model.FaxNumber;
                        dblocations.IsActive = model.IsActive;
                        dblocations.Notes = model.Notes;

                        if (FooterFileName != "")
                            dblocations.BrandFooterFilePath = FooterFileName;

                        if (HeaderFileName != "")
                            dblocations.BrandHeaderFilePath = HeaderFileName;

                        if (ApplicationFileName != "")
                            dblocations.ApplicationHeaderFilePath = ApplicationFileName;
                        DBContext.SaveChanges();

                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //var name = FileUpload1.FileName;
            return RedirectToAction("ViewSites", "SystemAdmin");
        }
        #endregion

        #region "changepassword"
        public ActionResult ChangePassWord()
        {
            return View(new ChangePasswordModel());
        }
        //[HttpPost]
        //public ActionResult ChangePassWord(FormCollection collection)
        //{
        //    string OldPass = collection["txtold"];
        //    string NewPass = collection["txtNew"];
        //    string ConfirmPass = collection["txtConfirm"];
        //    return View(new ChangePasswordModel());
        //}

        [HttpPost]
        public ActionResult ChangePassWord(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                return View(model);
            }
            else
            {
                return View(model);
            }

        }

        #endregion

        public ActionResult RunQuery()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RunQuery(FormCollection collection)
        {
            DataSet ds;
            Regex oRegEx;
            Match oRegMatch;
            ds = new DataSet();

            string sRestrictedSQLCommands = "update|insert|delete|create|alter|drop|exec|dbcc|create|truncate|deallocate|dump|execute|replace";
            oRegEx = new Regex(sRestrictedSQLCommands);
            string query = collection["txtquery"];

            if (query != "")
            {
                oRegMatch = oRegEx.Match(query.ToLower());
                if (oRegMatch.Success == false)
                    ds = GetViewData(query);
            }


            return View(ds);
        }

        SqlConnection _con;
        SqlCommand _cmd;
        SqlDataAdapter _da;
        DataSet _ds;
        SqlDataReader _reader;

        public DataSet GetViewData(string querry)
        {

            try
            {
                TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1();
                _con = (SqlConnection)DBContext.Database.Connection; //new SqlConnection("Data Source =10.10.12.132;Initial Catalog=PHdata_NewDb;User Id=sa;Password=cat@123?;");


                _cmd = new SqlCommand
                {

                    CommandText = querry,
                    CommandType = CommandType.Text,
                    Connection = _con
                };

                _con.Open();

                _da = new SqlDataAdapter(_cmd);
                _ds = new DataSet();
                _da.Fill(_ds);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }
            finally
            {
                _con.Close();
            }

            return _ds;
        }

        public ActionResult UserCredentials()
        {
            List<PersonModel> lstPersonModel = new List<PersonModel>();
            return View(lstPersonModel);
        }
        [HttpPost]
        public ActionResult UserCredentials(FormCollection collection)
        {

            List<PersonModel> lstPersonModel = new List<PersonModel>();
            string firstname = collection["txtfirstname"];
            string lastname = collection["txtlastname"];

            if (!string.IsNullOrEmpty(firstname) || !string.IsNullOrEmpty(lastname))
            {



                try
                {


                    ViewBag.firstname = firstname;
                    ViewBag.lastname = lastname;

                    using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                    {
                        List<TBScripts.DAL.Person> dbPersons = new List<TBScripts.DAL.Person>();
                        if (!String.IsNullOrEmpty(firstname))
                            dbPersons = DBContext.People.Where(location => location.FirstName.Contains(firstname)).ToList();
                        else if (!String.IsNullOrEmpty(lastname))
                            dbPersons = DBContext.People.Where(location => location.LastName.Contains(lastname)).ToList();

                        foreach (var item in dbPersons)
                        {
                            PersonModel cust = new PersonModel();
                            cust.siteId = item.SiteId;
                            cust.siteName = item.Site.SiteName;
                            cust.firstName = item.FirstName;
                            cust.lastName = item.LastName;
                            cust.loginId = item.LoginId;

                            cust.password = item.Password;
                            cust.emailAddress = item.EmailAddress;
                            cust.isActive = (bool)item.IsActive;
                            lstPersonModel.Add(cust);
                        }
                    }

                }

                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting  
                            // the current instance as InnerException  
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

            return View(lstPersonModel);
        }

        public ActionResult Books()
        {
            List<TBScripts.Models.BookModel> Books = new List<TBScripts.Models.BookModel>();
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    Books = DBContext.Books.Select(x => new BookModel
                    {
                        strBookTitle = x.BookTitle,
                        intBookId = x.BookId
                    }).ToList();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View(Books);
        }

        public ActionResult Sections(int BookId, string BookTitle)
        {
            BookSectionModel BookSections = new BookSectionModel();
            BookSections.BookId = BookId;
            BookSections.BookTitle = BookTitle;

            List<TBScripts.Models.SectionModel> Sections = new List<TBScripts.Models.SectionModel>();
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    Sections = DBContext.Sections.Where(x => x.BookId == BookId).Select(x => new SectionModel
                    {
                        BookId = x.BookId,
                        BookTitle = x.Book.BookTitle,
                        SectionId = x.SectionId,
                        SectionTitle = x.SectionTitle,
                        SortSequence = x.SortSeq,
                    }).ToList();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            BookSections.Sections = Sections;
            return View(BookSections);
        }
        [HttpGet]
        public ActionResult InsertSection(int SectionId, int BookId, string BookTitle)
        {
            TBScripts.Models.SectionModel Section = new TBScripts.Models.SectionModel();
            Section.BookId = BookId;
            Section.BookTitle = BookTitle;
            try
            {
                if (SectionId > 0)
                {
                    using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                    {
                        Section = DBContext.Sections.Where(x => x.SectionId == SectionId).Select(x => new SectionModel
                        {
                            BookId = x.BookId,
                            BookTitle = x.Book.BookTitle,
                            SectionId = x.SectionId,
                            SectionTitle = x.SectionTitle,
                            SortSequence = x.SortSeq,
                        }).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return PartialView("_SaveSection", Section);

        }
        [HttpPost]
        public ActionResult InsertSection(SectionModel model)
        {
            var Sections = new Section();
            try
            {
                if (ModelState.IsValid)
                {
                    using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                    {
                        if (model.SectionId > 0)
                        {
                            Sections = DBContext.Sections.Where(x => x.SectionId == model.SectionId).FirstOrDefault();
                            Sections.BookId = model.BookId;
                            Sections.SectionTitle = model.SectionTitle;
                            Sections.SortSeq = model.SortSequence;
                            DBContext.Entry(Sections).State = System.Data.Entity.EntityState.Modified;
                            DBContext.SaveChanges();
                        }
                        else
                        {
                            Sections.BookId = model.BookId;
                            Sections.SectionTitle = model.SectionTitle;
                            Sections.SortSeq = model.SortSequence;
                            DBContext.Sections.Add(Sections);
                            DBContext.SaveChanges();
                        }
                    }
                }

                return RedirectToAction("Sections", new { model.BookId, model.BookTitle });
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ActionResult DeleteSection(int SectionId, int BookId, string BookTitle)
        {
            var Sections = new Section();
            try
            {
                if (ModelState.IsValid)
                {
                    using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                    {
                        if (SectionId > 0)
                        {
                            Sections = DBContext.Sections.Where(x => x.SectionId == SectionId).FirstOrDefault();
                            //  DBContext.Entry(Sections).State = System.Data.Entity.EntityState.Deleted;
                            DBContext.Sections.Remove(Sections);
                            DBContext.SaveChanges();
                        }

                    }
                }

                return RedirectToAction("Sections", new { BookId, BookTitle });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Site Books

        public ActionResult SiteBooks(int SiteId, string SiteName)
        {


            List<TBScripts.Models.SiteBookModel> siteBooks = new List<TBScripts.Models.SiteBookModel>();
            SiteBookListModel siteBooksList = new SiteBookListModel();
            siteBooksList.SiteId = SiteId;
            siteBooksList.SiteName = SiteName;
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    siteBooks = DBContext.SiteBooks.Where(x => x.SiteId == SiteId).Select(x => new SiteBookModel
                    {
                        BookId = x.BookId,
                        BookTitle = x.Book.BookTitle,
                        SiteId = x.SiteId,
                        SiteName = SiteName

                    }).ToList();
                }

                siteBooksList.SiteBooks = siteBooks;

            }

            catch (Exception ex)
            {

                throw ex;
            }

            return View(siteBooksList);
        }
        [HttpGet]
        public PartialViewResult PartialViewAddSiteBooks(int SiteId, string SiteName, int BookId)
        {

            TBScripts.Models.SiteBookModel SiteBooks = new TBScripts.Models.SiteBookModel();

            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    if (BookId > 0)
                    {
                        SiteBooks = DBContext.SiteBooks.Where(x => x.BookId == BookId && x.SiteId == SiteId).Select(x => new SiteBookModel
                        {
                            BookId = x.BookId,
                            BookTitle = x.Book.BookTitle,
                            OldBookId = x.BookId,
                            //SiteId = x.SiteId,
                            //SiteName = SiteName,

                        }).FirstOrDefault();


                    }
                    if (SiteBooks == null)
                    {
                        SiteBooks = new TBScripts.Models.SiteBookModel();
                        SiteBooks.SiteId = SiteId;
                        SiteBooks.SiteName = SiteName;
                        SiteBooks.Books = DBContext.Books.ToList();
                    }
                    else
                    {
                        SiteBooks.SiteId = SiteId;
                        SiteBooks.SiteName = SiteName;
                        SiteBooks.Books = DBContext.Books.ToList();
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


            return PartialView("_AddSiteBookPartial", SiteBooks); // returns view with model
        }

        [HttpPost]
        public ActionResult PartialViewAddSiteBooks(int SiteId, int BookId, int OldBookId, string SiteName)
        {

            TBScripts.DAL.SiteBook SiteBooks = new TBScripts.DAL.SiteBook();

            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    if (OldBookId > 0)
                    {
                        if (BookId != OldBookId)
                        {
                            SiteBooks = DBContext.SiteBooks.Where(x => x.BookId == OldBookId && x.SiteId == SiteId).FirstOrDefault();
                            // SiteBooks.BookId = BookId;
                            // DBContext.Entry(SiteBooks).State = System.Data.Entity.EntityState.Modified;
                            DBContext.SiteBooks.Remove(SiteBooks);
                            DBContext.SaveChanges();
                        }



                    }

                    SiteBooks.SiteId = SiteId;
                    SiteBooks.BookId = BookId;
                    DBContext.SiteBooks.Add(SiteBooks);
                    DBContext.SaveChanges();


                }
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return RedirectToAction("SiteBooks", new { SiteId, SiteName });
            // return RedirectToAction("Sections", new { model.BookId, model.BookTitle });

        }
        [HttpGet]
        public JsonResult IsBookExistForSite(int SiteId, int BookId)
        {
            int booksCount = 0;
            try
            {

                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {
                    booksCount = DBContext.SiteBooks.Where(x => x.SiteId == SiteId && x.BookId == BookId).Count();
                }

            }

            catch (Exception ex)
            {

                throw ex;
            }

            return Json(booksCount, JsonRequestBehavior.AllowGet);
        }


    }
}