﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBScripts.Models;
using TBScripts.DAL;

namespace TBScripts.Controllers
{
    public class PersonController : Controller
    {
        private PersonDalc objpersonDal;
        private PersonModel objPerson;
        //For Log in


        public ActionResult Login(string returnUrl)
        {

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login", "Person");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(PersonModel model)
        {
            objpersonDal = new PersonDalc();
            objPerson = new PersonModel();
            string strUsername = model.loginId;
            string strPassword = model.password;
            if (strUsername != "" && strPassword != "")
            {
                objPerson.loginId = strUsername;
                objPerson.password = strPassword;
                objpersonDal.GetPersonByLoginId(ref objPerson);
                if (objPerson.personId > 0)
                {
                    Session["Person"] = objPerson;
                    Session["SiteId"] = objPerson.siteId;
                    Session["RoleName"] = objPerson.roleName;
                    Session["Username"] = strUsername;
                    Session["UserId"] = objPerson.personId;
                    return RedirectToAction("MainHome", "Home");
                }
            }

            // If we got this far, something failed, redisplay form
            return View();
        }

       

        public ActionResult ViewSystemUser()
        {

            List<PersonModel> lstPersonModel = new List<PersonModel>();
            try
            {


                using (TBScripts.DAL.PHdataConnectionString1 DBContext = new TBScripts.DAL.PHdataConnectionString1())
                {

                    // var dblocations = DBContext.ProductLines.Where(location => location.IsActive == true).ToList();
                    var dbPerson = from p in DBContext.People
                                        join s in DBContext.Sites on p.SiteId equals s.SiteId
                                        join r in DBContext.Roles on p.RoleId equals r.RoleId
                                        where s.IsActive==true
                                        select p;
                    foreach (var item in dbPerson)
                    {
                        PersonModel Persn = new PersonModel();
                        Persn.personId = item.PersonId;
                        Persn.firstName = item.FirstName;
                        Persn.lastName = item.LastName;
                        Persn.siteName = item.Site.SiteName;
                        Persn.emailAddress = item.EmailAddress;
                        Persn.loginId = item.LoginId;
                        Persn.isActive = item.IsActive;
                        Persn.roleName = item.Role.RoleName;

                        lstPersonModel.Add(Persn);
                    }
                }

            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            catch (Exception ex)
            {

                throw ex;
            }



            return View(lstPersonModel);
        }

        //Forgot Password

        public ActionResult ForgotPassword()
        {
            return View();
        }

        


        
    }
}
