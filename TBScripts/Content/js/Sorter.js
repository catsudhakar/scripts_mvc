﻿


$(document).ready(function ($) {
    //  alert('hi');
    $('#example').tablesorter({
        theme: 'blue',
        widthFixed: true,
        // widgets: ['filter']
    }).tablesorterPager({
        // target the pager markup - see the HTML block below
        container: $("#pager"),

        // output string - default is '{page}/{totalPages}';
        // possible variables:
        // {page}, {totalPages}, {startRow}, {endRow} and {totalRows}
        output: '{startRow} to {endRow} ({totalRows})',

        // apply disabled classname to the pager arrows when
        // the rows at either extreme is visible - default is true
        updateArrows: true,

        // remove rows from the table to speed up the sort of large
        // tables. setting this to false, only hides the non-visible rows;
        // needed if you plan to add/remove rows with the pager enabled.
        removeRows: false

    });

    //$("#example").tablesorter({ widthFixed: true, widgets: ['zebra'] }).tablesorterPager({ container: $("#pager") });


});





