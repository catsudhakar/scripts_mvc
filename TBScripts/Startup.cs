﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TBScripts.Startup))]
namespace TBScripts
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
