﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;



namespace TBScripts.Models
{
    public class TreeViewModel
    {
        //ProductLineId,ProductLineDesc,BookId,BookTitle,SiteId,SectionId,SectionTitle,ChapterId,ChapterTitle

        public int intSiteId { get; set; }
        public int intProductLineId { get; set; }
        public string strProductLineDesc { get; set; }

        public int intBookId { get; set; }
        public string strBookTitle { get; set; }

        public int intSectionId { get; set; }
        public string strSectionTitle { get; set; }

        public int intChapterId { get; set; }
        public string strChapterTitle { get; set; }

        public bool blnHasSections { get; set; }
        public bool blnHasChapters { get; set; }

        public List<HandoutModel> lstHandOutModel { get;set; }
        //public DataSet dsTree { get; set; }
    }
    public class HandoutModel
    {
        public int intHandOutId { get; set; }
        public string strName { get; set; }
        public string strkeywords { get; set; }
        public int intsectionId { get; set; }
        public int intlanguageId { get; set; }
        public string strstatusName { get; set; }
        public string strHtmlstring { get; set; }

    }

    public class ProductModel
    {
        public int intProductLineId { get; set; }
        public string strProductLineDesc { get; set; }
        public int intProductLineTypeId { get; set; }
        public string strProductLineTypeDesc { get; set; }
        public string CopyrightDesc { get; set; }
        public bool blnHasSections { get; set; }
        public bool blnHasChapters { get; set; }
        public int intlanguageId { get; set; }
        public string strLanguageName { get; set; }

        public string strDisclaimer { get; set; }

        public List<ddlProductLineTypeModel> LstProductLineTypeModel { get; set; }

        public IEnumerable<SelectListItem> ProductLineTypeItems
        {
            get { return new SelectList(LstProductLineTypeModel, "intProductLineTypeId", "strProductLineTypeDesc"); }
        }

    }
    public class ddlProductLineTypeModel
    {
        public int intProductLineTypeId { get; set; }
        public string strProductLineTypeDesc { get; set; }
    }
    public class Mymodel
    {
        public List<TreeViewModel> treeViewModel { get; set; }
        public HandoutModel HandoutModel { get; set; }
        public ProductModel ProductModel { get; set; }

        public string Name { get; set; }
        public int Id { get; set; }

    }
}