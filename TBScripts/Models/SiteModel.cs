﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TBScripts.Models
{
    public class SiteModel
    {
        [Display(Name = "Site Id")]
        public int SiteId { get; set; }
        [Required]
        [Display(Name = "Site Name")]
        public string SiteName { get; set; }
        [Required]
        [Display(Name = "Customer Name")]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Sitenumber must be a number")]
        [Display(Name = "Site Number")]
        public string SiteNumber { get; set; }
        public string BrandFooterFilePath { get; set; }
        public Nullable<int> BrandTypeId { get; set; }
        public string BrandHeaderFilePath { get; set; }
        public string ApplicationHeaderFilePath { get; set; }
        public bool SuppressOurBrand { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string StateName { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public bool IsActive { get; set; }
        public string Notes { get; set; }

        //public virtual BrandType BrandType { get; set; }
        //public virtual Customer Customer { get; set; }
        //public virtual ICollection<Person> People { get; set; }
        //public virtual StateProvince StateProvince { get; set; }
        //public virtual ICollection<ProductLine> ProductLines { get; set; }

        public virtual List<BrandTypeModel> lstBrandType { get; set; }
        public virtual List<CustomerModel> lstCustomer { get; set; }
        public virtual ICollection<PersonModel> People { get; set; }
        public virtual List<StateModel> lstStateProvince { get; set; }
        // public virtual ICollecti> ProductLines { get; set; }
    }
}