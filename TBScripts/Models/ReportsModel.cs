﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TBScripts.Models
{
    public class ReportsModel
    {
        public int personId { get; set; }
        public string firstName { get; set; }

        public string lastName { get; set; }

        public int siteId { get; set; }
        public string siteName { get; set; }

        public string loginId { get; set; }

        public int LanguageId { get; set; }
        public int ViewType { get; set; }

        public string Jan { get; set; }
        public string Feb { get; set; }
        public string March { get; set; }
        public string April { get; set; }

        public string May { get; set; }
        public string June { get; set; }
        public string July { get; set; }
        public string Aug { get; set; }

        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }

    }

    public class SectionChapters
    {
        public int SectionId { get; set; }
        public string SectionTitle { get; set; }
        public int ChapterId { get; set; }
        public string ChapterTitle { get; set; }
        public int ChapterSort { get; set; }
    }

    public class ChapterHandOuts
    {
        public int ChapterId { get; set; }
        public string ChapterTitle { get; set; }
        public int HandoutId { get; set; }
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string HandoutTitle { get; set; }
        public string ChangeNotes { get; set; }
        public string DatePublished { get; set; }
        public int HandoutStatusId { get; set; }
        public bool IsPublic { get; set; }
    }
    public class ReportListModel
    {
        public SelectList BookList { get; set; }
        public List<SiteModel> SiteList { get; set; }
        public List<PreNotifyListModel> PreNotifyList { get; set; }
    }

    public class FileStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int status { get; set; }
    }

    public class PreNotifyListModel
    {

        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public string AnticipatedPublishDate { get; set; }

        public int SectionId { get; set; }
        public string SectionTitle { get; set; }
        public int SectionSort { get; set; }
        public int ChapterId { get; set; }
        public string ChapterTitle { get; set; }
        public int ChapterSort { get; set; }

        public int HandoutId { get; set; }
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string HandoutTitle { get; set; }
        public string ChangeNotes { get; set; }
        public string DatePublished { get; set; }
        public int HandoutStatusId { get; set; }
        public bool IsPublic { get; set; }

        public List<FileStatusModel> FileStatus { get; set; }



    }

    public class TotalUsageStatusReport
    {
        public SelectList Languages { get; set; }
        public SelectList  SiteList { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<HandoutCounts> HandoutCounts { get; set; }

    }

    public class HandoutCounts
    {
        public int HandoutId { get; set; }
        public string HandoutTitle { get; set; }
        public int ViewsCount { get; set; }
        public int  PrintCounts { get; set; }
       
    }

    public class SiteReport
    {
        public int SiteId { get; set; }
        public string SiteTitle { get; set; }
       

    }
}