﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using TBScripts.DAL;

namespace TBScripts.Models
{
    public class PersonModel
    {
        public int personId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        [Required]
        [Display(Name = "Site")]
        public int siteId { get; set; }
        public string siteName { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        [EmailAddress]
        public string emailAddress { get; set; }



        [Required]
        [Display(Name = "LogIn")]
        public string loginId { get; set; }

        [Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }

        [Display(Name = "Role")]
        [Required]

        public int roleId { get; set; }
        public string roleName { get; set; }
        public bool isActive { get; set; }
        [Display(Name = "Phone Type1")]
        public int  ? phone1TypeId { get; set; }
        [Display(Name = "Phone Type2")]
        public int  ? phone2TypeId { get; set; }
        [Display(Name = "Phone Type3")]
        public int ? phone3TypeId { get; set; }
        [Display(Name = "Phone Type4")]
        public int ? phone4TypeId { get; set; }
        [Display(Name = "Phone Number1")]
        public string phoneNumber1 { get; set; }
        [Display(Name = "Phone Number2")]
        public string phoneNumber2 { get; set; }
        [Display(Name = "Phone Number3")]
        public string phoneNumber3 { get; set; }
        [Display(Name = "Phone Number4")]
        public string phoneNumber4 { get; set; }
        [Display(Name = "Notes")]
        public string notes { get; set; }

        public virtual ICollection<HandoutArchive> HandoutArchives { get; set; }
        public virtual ICollection<HandoutLanguage> HandoutLanguages { get; set; }
        public virtual ICollection<HandoutUsage> HandoutUsages { get; set; }
        public virtual PhoneType PhoneType { get; set; }
        public virtual PhoneType PhoneType1 { get; set; }
        public virtual PhoneType PhoneType2 { get; set; }
        public virtual PhoneType PhoneType3 { get; set; }
        public virtual Role Role { get; set; }
        //public virtual Site Site { get; set; }
        public List<Site> lstSite { get; set; }
        public List<PhoneType> lstPhoneType { get; set; }
        public List<Role> lstRole { get; set; }


    }

    public class ChangePasswordModel
    {
        [Required]
        [Display(Name = "Old Password")]
        public string password { get; set; }

        [Required]
        [Display(Name = "New Password")]
        public string newPassword { get; set; }

        [Required]
        [Compare("newPassword")]
        [Display(Name = "Confirm Password")]
        public string confirmPassword { get; set; }

        public string message { get; set; }
    }
}