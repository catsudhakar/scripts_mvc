﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TBScripts.DAL;

namespace TBScripts.Models
{
    public class LanguageModel
    {
        public int intlanguageId { get; set; }
        public string strlanguageName { get; set; }
    }

     public class PhoneTypeModel
    {
        public int PhoneTypeId { get; set; }
        public string PhoneTypeDesc { get; set; }

    }

    public class RecentTypeModel
    {
        public int RecentTypeId { get; set; }
        public string RecentTypeDesc { get; set; }
        public byte KeepCount { get; set; }
    }
   
  

    public class ProductLineTypeModel
    {
        public int intProductLineTypeId { get; set; }
        public string strProductLineTypeDesc { get; set; }
        public bool blnHasSections { get; set; }
        public bool blnHasChapters { get; set; }

        public int intlanguageId { get; set; }
        public string strlanguageName { get; set; }

        public int intProductLineId { get; set; }
        public string strProductLineDesc { get; set; }
        public string strCopyrightDesc { get; set; }

        public int intDisclaimerId { get; set; }
        public string strDisclaimerName { get; set; }
    }

    public class ProductLineModel
    {
        public int ProductLineId { get; set; }
        public string ProductLineDesc { get; set; }
        public int ProductLineTypeId { get; set; }
        public string strProductLineTypeDesc { get; set; }
        public string CopyrightDesc { get; set; }

        public List<ProductLineTypeModel> ProductLineType { get; set; }

    }

    public class ListProductLineTypeModel
    {
        public List<ProductLineModel> lstProductLine { get; set; }
        public List<ProductLineType> lstProductLineType { get; set; }
        public string LineType { get; set; }

    }
}