﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TBScripts.Models
{
    public class StateModel
    {
        public string StateId { get; set; }
        public string StateName { get; set; }
    }

    public class BrandTypeModel
    {
        public int BrandTypeId { get; set; }
        public string BrandTypeDesc { get; set; }

        public ICollection<SiteModel> Sites { get; set; }
    }
}