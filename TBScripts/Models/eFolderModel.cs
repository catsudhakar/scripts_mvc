﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TBScripts.Models
{
    public class eFolderModel
    {
        public byte[] pdf { get; set; }
        public int SiteId { get; set; }
        public string SiteName{ get; set; }
     

        public int FolderId { get; set; }
        public string FolderName { get; set; }
          

        public int HandoutId { get; set; }
        public string HandoutName { get; set; }

        public int SortSeq { get; set; }
        public string KeywordList { get; set; }
        public string NavigationTitle { get; set; }

     
        

        public List<FolderFiles> folderfileslist { get; set; }
    }

    public class FolderFiles
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Type { get; set; }
    }

    public class MyeFolderModel
    {
        public List<eFolderModel> lsteFolderModel { get; set; }
        public string m_sRootNodeTile { get; set; }
        public bool m_bShowEditLinks { get; set; }
      //  public List<eFolderModel> efolderViewModel { get; set; }
      

    }

    public class PDFViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }


    }
}