﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace TBScripts.Models
{
    public class NotesModel
    {


        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        [Display(Name = "Page Content")]
        public string PageContent { get; set; }

        public int Id { get; set; }
        public int FileId { get; set; }
        public int SiteId { get; set; }
        [Required]
        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        [Display(Name = "Text")]
        public string Text { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string Description { get; set; }
        public bool isDeleted { get; set; }

    }
}