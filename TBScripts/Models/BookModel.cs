﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TBScripts.Models
{
    public class BookModel
    {
        public int intBookId { get; set; }
        public string strBookTitle { get; set; }

        public int intProductLineId { get; set; }
        public string ProductLineName { get; set; }
        public int intBookTypeId { get; set; }

        public DateTime? ChangesDeadline { get; set; }
        public DateTime? AnticipatedPublishDate { get; set; }
        public DateTime? NotificationSentDate { get; set; }
        public DateTime? DatePublished { get; set; }
    }

    public class SectionModel
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }

        public int SectionId { get; set; }
        [Required]
        [Display(Name = "Section Title")]
        [StringLength(30)]
        public string SectionTitle { get; set; }
        [Display(Name = "Sort Sequence")]
        [Range(0, 100, ErrorMessage = "Please enter valid integer Number")]
        public int? SortSequence { get; set; }

        public int SiteId { get; set; }

        public List<BookModel> Books { get; set; }

    }

    public class BookSectionModel
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public List<SectionModel> Sections { get; set; }
    }

    public class SiteBookModel
    {
        [Required]
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        [Required]
        public int SiteId { get; set; }
        public string SiteName { get; set; }

        public int OldBookId { get; set; }

        public List<TBScripts.DAL.Book> Books { get; set; }
    }

    public class SiteBookListModel
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }

        public List<SiteBookModel> SiteBooks { get; set; }
    }

}