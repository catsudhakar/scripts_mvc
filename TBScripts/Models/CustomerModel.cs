﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TBScripts.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }

        [Required]
        [Display(Name = "Customer Name")]
        [StringLength(30)]
        public string CustomerName { get; set; }

        public bool IsActive { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string StateId { get; set; }
        [Required]
        public string PostalCode { get; set; }
        public string Notes { get; set; }

        public List<StateModel> StatesList { get; set; }

        //public virtual ICollection<Site> Sites { get; set; }
        //public virtual StateProvince StateProvince { get; set; }
    }
}