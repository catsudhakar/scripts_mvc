﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace TBScripts.Models
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string urole = null;
        public string[] uroles = null;
        public string userRoleName = HttpContext.Current.Session["RoleName"].ToString();

        public CustomAuthorizeAttribute(string userRole)
        {
            urole = userRole;
        }

        public CustomAuthorizeAttribute(string[] userRoles)
        {
            uroles = userRoles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);

            //if (urole != HttpContext.Current.Session["RoleName"])
            //{
            //    // The user is not authorized => no need to go any further
            //    //return RedirectToAction("MainHome", "Home");
            //    return false;
            //}
            //return true;
           if(uroles.Contains(userRoleName))
           {
               return true;
           }

            return false;
        }
    }
}